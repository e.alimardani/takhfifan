import {StyleSheet} from 'react-native';
export const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#fff',
  },
  ProfileItem: {
    width: '100%',
    borderRadius: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#FFFF',
    elevation: 2,
    height: 50,
    paddingHorizontal: 10,
    marginBottom: 10,
  },
  ImageProfile: {
    alignSelf: 'center',
    width: 100,
    height: 100,
    borderRadius: 50,
  },
  content: {
    backgroundColor: 'transparent',
    margin: 20,
    flex: 1,
    padding: 20,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  TextName: {
    textAlign: 'center',
    color: 'black',
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  Font16: {fontSize: 16},
});

export default styles;
