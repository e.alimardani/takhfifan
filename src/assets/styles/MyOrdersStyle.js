import {StyleSheet} from 'react-native';
export const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#0082f0',
  },
  Header: {
    backgroundColor: '#0082f0',
    elevation: 0,
    marginTop: 20,
  },
  TextHeader: {
    color: 'white',
    marginRight: 15,
    fontWeight: 'bold',
    fontSize: 18,
  },
  IconHeader: {
    marginLeft: 15,
    color: 'white',
  },
  ContentStyle: {
    flex: 1,
    backgroundColor: '#fff',
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    paddingVertical: 20,
  },
  WrapperList: {
    alignItems: 'center',

    flex: 1,
    flexDirection: 'row',
    padding: 10,
    marginHorizontal: 15,
    marginBottom: 10,
    elevation: 3,
    backgroundColor: '#FFFF',
    borderRadius: 5,
    marginTop: 5,
  },
  ImgList: {
    flex: 1,
    height: 100,
  },
  ListData: {
    paddingHorizontal: 10,
    flex: 2,
    height: '100%',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  WrapperPrice: {
    flexDirection: 'row-reverse',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderTopColor: '#eaeaea',
    borderTopWidth: 1,
    paddingTop: 5,
  },
  Empty: {
    padding: 15,
    marginHorizontal: 15,
  },
  TextEmpty: {
    textAlign: 'center',
    fontSize: 17,
  },
  Price: {},
  PriceNew: {color: '#0082f0', fontSize: 17},
  PriceOld: {textDecorationLine: 'line-through'},
  BuyCount: {flexDirection: 'row-reverse', alignItems: 'center'},
  BuyCountText: {},
  BuyCountIcon: {marginRight: 5, fontSize: 18},
  WrapperTitle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  OffText: {
    color: 'white',
    backgroundColor: '#0082f0',
    width: 35,
    height: 35,
    fontSize: 13,
    borderRadius: 17.5,
    textAlignVertical: 'center',
    textAlign: 'center',
  },
  Title: {},
});

export default styles;
