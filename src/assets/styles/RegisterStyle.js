import {Platform, Dimensions, PixelRatio, StyleSheet} from 'react-native';

const width = Dimensions.get('window').width;
export const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#eee',
  },
  mainBackground: {
    position: 'absolute',
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    top: 0,
    //opacity:0.7
  },
  logoSection: {
    flex: 0.4,
    justifyContent: 'flex-end',
    alignItems: 'center',
    paddingBottom: 30,
  },
  logoImage: {
    width: width * 0.5,
    height: width * 0.2,
    // backgroundColor:'#000',
    alignSelf: 'center',
  },
  logoSlogan: {
    textAlign: 'center',
    writingDirection: 'rtl',
    fontSize: 14,
    color: '#eee',
  },
  splashBottomSection: {
    backgroundColor: 'transparent',
    position: 'absolute',
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height * 0.25,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  grButton: {
    alignSelf: 'center',
    backgroundColor: '#eee',
    width: 200,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
    ...Platform.select({
      ios: {
        shadowColor: 'rgb(0, 0, 0)',
        shadowOffset: {width: 0, height: 0},
        shadowOpacity: 0.2,
        shadowRadius: 5,
      },
      android: {
        elevation: 2,
      },
    }),
  },
  colorGrButton: {
    alignSelf: 'center',
    backgroundColor: '#eee',
    width: 200,
    height: 40,
    marginTop: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
    ...Platform.select({
      ios: {
        shadowColor: '#eee',
        shadowOffset: {width: 0, height: 0},
        shadowOpacity: 0.6,
        shadowRadius: 7,
      },
      android: {
        elevation: 2,
      },
    }),
  },
  grButtonGradient: {
    width: 200,
    height: 40,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  grButtonText: {
    textAlign: 'center',
    writingDirection: 'rtl',
    fontSize: 14,
    //lineHeight:14,
    color: '#ffffff',
  },
  buttonTransparentText: {
    textAlign: 'center',
    writingDirection: 'rtl',
    fontSize: 14,
    //lineHeight:14,
    color: 'black',
    fontFamily: 'IRANSansMobile',
  },
  swiperContainer: {
    //flex:1,
    backgroundColor: 'transparent',
    ...Platform.select({
      android: {
        width: Dimensions.get('window').width,
      },
    }),
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 20,
    height: Dimensions.get('window').height * 0.75,
  },
  slide: {
    width: Dimensions.get('window').width - 80,
    backgroundColor: '#ffffff',
    padding: 10,
    paddingHorizontal: 20,
    marginHorizontal: 40,
    borderRadius: 20,
    ...Platform.select({
      ios: {
        shadowColor: 'rgb(0, 0, 0)',
        shadowOffset: {width: 0, height: 0},
        shadowOpacity: 0.3,
        shadowRadius: 6,
      },
      android: {
        elevation: 5,
      },
    }),
    marginTop: 0,
    //height:Dimensions.get('window').height,
  },
  dot: {
    backgroundColor: '#393939',
    width: 8,
    height: 8,
    borderRadius: 4,
    marginLeft: 3,
    marginRight: 3,
    marginTop: 3,
    marginBottom: 3,
  },
  activeDot: {
    backgroundColor: 'rgba(0,0,0,0.3)',
    width: 8,
    height: 8,
    borderRadius: 4,
    marginLeft: 3,
    marginRight: 3,
    marginTop: 3,
    marginBottom: 3,
  },
  //Input
  inputContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 5,
  },
  inputLabel: {
    textAlign: 'center',
    writingDirection: 'rtl',
    fontSize: 10,
    //lineHeight:12,
    color: '#aaaaaa',
  },
  inputMain: {
    borderBottomColor: '#eeeeee',
    borderBottomWidth: 4 / PixelRatio.get(),
    width: '100%',
    textAlign: 'center',
    writingDirection: 'rtl',
    fontSize: 14,
    //lineHeight:14,
    color: '#444444',
    paddingVertical: 2,
    paddingHorizontal: 10,
  },
  eyeButton: {
    position: 'absolute',
    top: 18,
    left: 5,
  },
  eyeButtonIcon: {
    fontSize: 24,
    color: '#eee',
  },
});

export default styles;
