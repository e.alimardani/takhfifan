import {StyleSheet, Dimensions, Platform, PixelRatio} from 'react-native';

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#eee',
  },
  SubmitBtn: {
    backgroundColor: '#aaa',
    marginTop: 10,
    elevation: 5,
  },
  ModalError: {
    backgroundColor: '#fff',
    borderRadius: 10,
    height: 200,
    width: '80%',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  waitText: {
    marginTop: 10,
  },
  TextDsc: {marginTop: 10, fontSize: 16, textAlign: 'center'},
  colorGrButton: {
    alignSelf: 'center',
    backgroundColor: '#eee',
    width: 200,
    height: 40,
    marginTop: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
    ...Platform.select({
      ios: {
        shadowColor: '#eee',
        shadowOffset: {width: 0, height: 0},
        shadowOpacity: 0.6,
        shadowRadius: 7,
      },
      android: {
        elevation: 2,
      },
    }),
  },
  SubmitText: {
    textAlign: 'center',
    padding: 10,
  },
  grButtonText: {
    textAlign: 'center',
    writingDirection: 'rtl',
    fontSize: 14,
    //lineHeight:14,
    color: 'black',
  },
  grButtonGradient: {
    width: 200,
    height: 40,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputMain: {
    borderBottomColor: '#eeeeee',
    borderBottomWidth: 4 / PixelRatio.get(),
    width: '100%',
    textAlign: 'center',
    writingDirection: 'rtl',
    fontSize: 14,
    //lineHeight:14,
    color: '#444444',
    paddingVertical: 2,
    paddingHorizontal: 10,
  },
  slide: {
    width: Dimensions.get('window').width - 80,
    backgroundColor: '#ffffff',
    padding: 10,
    paddingHorizontal: 20,
    marginHorizontal: 40,
    borderRadius: 20,
    ...Platform.select({
      ios: {
        shadowColor: 'rgb(0, 0, 0)',
        shadowOffset: {width: 0, height: 0},
        shadowOpacity: 0.3,
        shadowRadius: 6,
      },
      android: {
        elevation: 5,
      },
    }),
    marginTop: 0,
  },
  inputContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 5,
  },
  inputLabel: {
    textAlign: 'center',
    writingDirection: 'rtl',
    fontSize: 10,
    //lineHeight:12,
    color: '#aaaaaa',
  },
});
export default styles;
