import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#0082f0',
    marginVertical: 20,
  },
  Header: {
    backgroundColor: '#0082f0',
    elevation: 0,
  },
  textCenter: {
    fontSize: 14,
    textAlign: 'center',
  },
  BottomRowCart: {
    flexDirection: 'row-reverse',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    paddingBottom: 5,
  },
  TextHeader: {
    color: 'white',
    marginRight: 15,
    fontWeight: 'bold',
    fontSize: 18,
  },
  WrapperCartIcon: {
    flexDirection: 'row',
  },
  CartIcon: {
    fontSize: 22,
    marginRight: 5,
  },
  carouselItem: {
    overflow: 'hidden',
    flex: 1,
  },
  IconHeader: {
    marginLeft: 15,
    color: 'white',
  },
  ImageCarousel: {
    alignSelf: 'center',
    width: 180,
    height: 250,
  },
  HeaderWrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
  ContentStyle: {
    flex: 1,
    backgroundColor: '#fff',
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    paddingTop: 20,
  },
  ImageHeader: {
    height: 50,
    width: 50,
    borderRadius: 25,
    alignItems: 'center',
    backgroundColor: '#FFFF',
    justifyContent: 'center',
  },
  renderHeader: {
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  CarouselWrapper: {
    paddingVertical: 15,
    backgroundColor: 'red',
  },
  TextRender: {
    fontSize: 17,
    textAlign: 'center',
    color: 'white',
  },
  TempTextOff: {
    fontWeight: 'bold',
    color: 'white',
    position: 'absolute',
    top: 5,
    left: 5,
    zIndex: 999999,
  },
  TempViewOff: {
    height: 90,
    width: 90,
    transform: [{rotate: '45deg'}],
    backgroundColor: 'red',
    position: 'absolute',
    top: -45,
    left: -45,
    zIndex: 9999,
  },
  TempView: {
    height: 55,
    opacity: 0.6,
    backgroundColor: '#eaeaea',
    position: 'absolute',
    bottom: 0,
    right: 0,
    left: 0,
  },
  WrapperRow: {
    paddingTop: 5,
    position: 'absolute',
    bottom: 0,
    right: 0,
    left: 0,
    zIndex: 999,
  },
  TopRowCart: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    paddingBottom: 5,
  },
  textCenterPrice: {
    fontSize: 14,
    textAlign: 'center',
  },
  OffText: {
    fontSize: 14,
    textAlign: 'center',
    textDecorationLine: 'line-through',
  },
  TextTitle: {
    fontSize: 14,
    flex: 1,
    textAlign: 'left',
  },
  FlatList: {
    marginVertical: 10,
    backgroundColor: 'transparent',
  },
  Carousel: {
    backgroundColor: '#fff',
    paddingBottom: 60,
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
  },
  HeadCarousel: {
    textAlignVertical: 'center',
    fontWeight: 'bold',
    fontSize: 17,
    color: '#6D7476',
  },
  WrapperHeadCarousel: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingHorizontal: 10,
    marginBottom: 30,
  },
  HeadCarouselWrap: {
    alignItems: 'center',
    flexDirection: 'row',
  },
  IconHead: {
    color: '#0082f0',
    fontSize: 20,
  },
  TextMore: {
    color: '#0082f0',
    fontSize: 14,
  },
});

export default styles;
