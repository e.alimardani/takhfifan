import {SET_USER, SET_TABBAR_INDEX} from './Type';
export const setUser = user => ({
  type: SET_USER,
  user,
});
export const changeIndex = index => ({
  type: SET_TABBAR_INDEX,
  index,
});
