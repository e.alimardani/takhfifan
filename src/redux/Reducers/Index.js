import {combineReducers} from 'redux';
import user from './userReducer';
import global from './globalReducer';
import Tab from './tabReducer';

export default combineReducers({
  user,
  global,
  Tab,
});
