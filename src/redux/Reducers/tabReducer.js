/* eslint-disable prettier/prettier */
import {SET_TABBAR_INDEX} from '../Actions/Type';

const initialState = {
  index: 2,
};

// eslint-disable-next-line no-undef
export default Tab = (state = initialState, action = {}) => {
  switch (action.type) {
    case SET_TABBAR_INDEX:
      return {
        index: action.index.index,
      };

    default:
      return state;
  }
};
