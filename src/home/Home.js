import React from 'react';
import {StatusBar, Text, Animated, Dimensions} from 'react-native';
import Add from './Add';
import Profile from './Profile';
import {
  Content,
  Container,
  Button,
  Footer,
  FooterTab,
  Icon,
  Header,
  Left,
  Right,
} from 'native-base';
import styles from '../assets/styles/HomeStyle';
import {setUser} from '../redux/Actions/Index';
import {connect} from 'react-redux';
import Main from './Main';
const width = Dimensions.get('window').width;
class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 'Main',
      removeAnim: new Animated.Value(0.8),
      off: false,
    };
  }
  Grow() {
    Animated.timing(this.state.removeAnim, {
      toValue: 1.3,
      duration: 800,
      useNativeDriver: true,
    }).start();
  }
  Less() {
    Animated.timing(this.state.removeAnim, {
      toValue: 1,
      duration: 800,
      useNativeDriver: true,
    }).start();
  }
  componentDidMount() {
    setInterval(() => {
      if (!this.state.off) {
        this.Grow(), this.setState({off: true});
      } else {
        this.Less();
        this.setState({off: false});
      }
    }, 800);
  }
  render() {
    return (
      <Container style={styles.mainContainer}>
        {!this.state.page === 'Main' && (
          <StatusBar
            backgroundColor="transparent"
            translucent={true}
            barStyle="dark-content"
          />
        )}
        {this.state.page === 'Main' && (
          <Header androidStatusBarColor={'#0082f0'} style={styles.Header}>
            <Left>
              <Icon style={styles.IconHeader} name="menu" />
            </Left>
            <Right>
              <Text style={styles.TextHeader}>MITIGATE</Text>
            </Right>
          </Header>
        )}
        <Content>
          {this.state.page === 'add' && <Add />}
          {this.state.page === 'Main' && <Main />}
          {this.state.page === 'profile' && <Profile />}
        </Content>
      </Container>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    setUser: user => {
      dispatch(setUser(user));
    },
  };
};

const mapStateToProps = state => {
  return {
    user: state.user,
    global: state.global,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Home);
