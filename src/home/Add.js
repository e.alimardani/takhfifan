import React from 'react';
import {
  StatusBar,
  TextInput,
  Text,
  TouchableOpacity,
  View,
  Image,
  ActivityIndicator,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Modal from 'react-native-modal';
import {Content, Container, Picker} from 'native-base';
import styles from '../assets/styles/HomeStyle';
import ImagePicker from 'react-native-image-crop-picker';
import {setUser} from '../redux/Actions/Index';
import DateTimePicker from 'react-native-modal-datetime-picker';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
class Add extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      IsModal: false,
      loading: false,
      hasError: false,
      errorDesc: 'Error occured',
      Title: this.props.Edit ? this.props.EditData.Title : '',
      Price: this.props.Edit
        ? JSON.stringify(this.props.EditData.NewPrice)
        : '',
      Percent: this.props.Edit
        ? JSON.stringify(this.props.EditData.Percent)
        : '',
      ExpireTime: this.props.Edit ? this.props.EditData.Expiretime : '',
      Description: this.props.Edit ? this.props.EditData.Description : '',
      IMG1: this.props.Edit
        ? {
            path:
              'http://luxuryglass.ir/Images/Ads/Img/' +
              this.props.EditData.MainImg,
            mime: 'jpg',
          }
        : '',
      IMG2: '',
      IMG3: '',
      IMG4: '',
      selected: this.props.Edit ? this.props.EditData.CategoryId : 1,
      CateGoryId: this.props.Edit ? this.props.EditData.ParentCategory : 'key0',
      SubCatId: '',
      lang: 1,
      avatarSource: '',
      imageAvatar: '',
      dataAvatar: '',
      nameImageAvatar: '',
      ImageUri1: this.props.Edit
        ? 'http://luxuryglass.ir/Images/Ads/Img/' + this.props.EditData.MainImg
        : '',
      ImageUri2: '',
      ImageUri3: '',
      ImageUri4: '',
      MainCategory: [
        {
          CategoryId: this.props.Edit
            ? this.props.EditData.ParentCategory
            : '0',
          CategoryName: this.props.Edit
            ? this.props.EditData.ParentCategoryName
            : 'Select Main Category',
        },
      ],
      SubCategory: [
        {
          CategoryId: this.props.Edit ? this.props.EditData.CategoryId : '0',
          CategoryName: this.props.Edit
            ? this.props.EditData.CategoryName
            : 'Select Sub Item',
        },
      ],
    };
  }

  async componentWillMount() {
    this.getMainCategory();
    console.log(this.props.EditData);
    console.log(this.props.Edit);
  }
  SubOnValueChange(value) {
    this.setState({
      selected: value,
    });
  }
  onValueChange(value) {
    this.setState(
      {
        CateGoryId: value,
      },
      () => this.getSubCategory(),
    );
  }
  ShowImagesEdit1() {
    return (
      <View>
        <View style={styles.Wrapperimage}>
          <TouchableOpacity
            onPress={() => this._openCamera1()}
            style={styles.image}>
            {this.props.EditData.MainImg === '' && (
              <Text style={styles.TextImg}>Add Image</Text>
            )}

            {this.props.EditData.MainImg !== '' && (
              <Image
                source={{
                  uri:
                    this.state.ImageUri1 !== ''
                      ? this.state.ImageUri1
                      : 'http://luxuryglass.ir/Images/Ads/Img/' +
                        this.props.EditData.MainImg,
                }}
                style={styles.imageSet}
              />
            )}
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  ShowImages1() {
    return (
      <View>
        <View style={styles.Wrapperimage}>
          <TouchableOpacity
            onPress={() => this._openCamera1()}
            style={styles.image}>
            {this.state.ImageUri1 === '' && (
              <Text style={styles.TextImg}>Add Image</Text>
            )}

            {this.state.ImageUri1 !== '' && (
              <Image
                source={{
                  uri: this.state.ImageUri1,
                }}
                style={styles.imageSet}
              />
            )}
          </TouchableOpacity>
        </View>
      </View>
    );
  }
  ShowImages2() {
    return (
      <View>
        <View style={styles.Wrapperimage}>
          <TouchableOpacity
            onPress={() => this._openCamera2()}
            style={styles.image}>
            {this.state.ImageUri2 === '' && (
              <Text style={styles.TextImg}>Add Image</Text>
            )}

            {this.state.ImageUri2 !== '' && (
              <Image
                source={{uri: this.state.ImageUri2}}
                style={styles.imageSet}
              />
            )}
          </TouchableOpacity>
        </View>
      </View>
    );
  }
  ShowImages3() {
    return (
      <View>
        <View style={styles.Wrapperimage}>
          <TouchableOpacity
            onPress={() => this._openCamera3()}
            style={styles.image}>
            {this.state.ImageUri3 === '' && (
              <Text style={styles.TextImg}>Add Image</Text>
            )}

            {this.state.ImageUri3 !== '' && (
              <Image
                source={{uri: this.state.ImageUri3}}
                style={styles.imageSet}
              />
            )}
          </TouchableOpacity>
        </View>
      </View>
    );
  }
  ShowImages4() {
    return (
      <View>
        <View style={styles.Wrapperimage}>
          <TouchableOpacity
            onPress={() => this._openCamera4()}
            style={styles.image}>
            {this.state.ImageUri4 === '' && (
              <Text style={styles.TextImg}>Add Image</Text>
            )}

            {this.state.ImageUri4 !== '' && (
              <Image
                source={{uri: this.state.ImageUri4}}
                style={styles.imageSet}
              />
            )}
          </TouchableOpacity>
        </View>
      </View>
    );
  }
  async AddAds(image1, image2, image3, image4) {
    if (this.state.IMG1.length === 0) {
      this.setState({
        IsModal: true,
        loading: false,
        errorDesc: 'Please Select a Image',
        hasError: true,
      });
    } else {
      this.setState({
        IsModal: true,
        loading: true,
        errorDesc: '',
        hasError: false,
      });
      try {
        const form = new FormData();
        const name1 = image1.path.split('/').reverse()[0];
        const name2 = image2.path.split('/').reverse()[0];
        const name3 = image3.path.split('/').reverse()[0];
        const name4 = image4.path.split('/').reverse()[0];
        form.append('Token', this.props.user.apiToken);
        form.append('Categoryid', JSON.stringify(this.state.selected));
        form.append('Title', this.state.Title);
        form.append('Price', this.state.Price);
        form.append('Percent', this.state.Percent);
        form.append('ExpireTime', this.state.ExpireTime);
        form.append('Image1', name1);
        form.append('Image2', name2);
        form.append('Image3', name3);
        form.append('Image4', name4);
        form.append('Description', this.state.Description);
        form.append('ImageFilBytes1', {
          uri: image1.path,
          name1,
          type: image1.mime,
        });
        form.append('ImageFilBytes2', {
          uri: image2.path,
          name2,
          type: image2.mime,
        });
        form.append('ImageFilBytes3', {
          uri: image3.path,
          name3,
          type: image3.mime,
        });
        form.append('ImageFilBytes4', {
          uri: image4.path,
          name4,
          type: image4.mime,
        });

        let response = await fetch(this.props.global.baseApiUrl + '/AddAds', {
          method: 'POST',
          body: form,
        });
        let json = await response.json();
        console.log(form);
        console.log(json);
        if (json === 'SUCCESS') {
          this.setState({
            IsModal: true,
            hasError: true,
            errorDesc: 'Success',
            loading: false,
            Title: '',
            Price: '',
            Percent: '',
            ExpireTime: '',
            Description: '',
            selected: '',
            ImageUri1: '',
            ImageUri2: '',
            ImageUri3: '',
            ImageUri4: '',
            IMG1: '',
            IMG2: '',
            IMG3: '',
            IMG4: '',
            isDateTimePickerVisible: false,
          });
        } else {
          this.setState({
            IsModal: true,
            hasError: true,
            errorDesc: 'Error',
            loading: false,
          });
        }
      } catch (error) {
        this.setState({
          IsModal: true,
          hasError: true,
          errorDesc: 'Error',
          loading: false,
        });
      }
    }
  }
  showDateTimePicker = () => {
    this.setState({isDateTimePickerVisible: true});
  };

  hideDateTimePicker = () => {
    this.setState({isDateTimePickerVisible: false});
  };

  handleDatePicked = date => {
    console.log('A date has been picked: ', date);
    var myDate = new Date(date);
    this.setState({ExpireTime: myDate.toLocaleString().split(',')[0]});
    this.hideDateTimePicker();
  };
  _openCamera1 = () => {
    ImagePicker.openCamera({
      width: 300,
      height: 400,
      cropping: true,
      compressImageQuality: 0.6,
    })
      .then(image => {
        this.setState({IMG1: image, ImageUri1: image.path});
      })
      .catch(err => {
        console.log(err);
      });
  };
  _openCamera2 = () => {
    ImagePicker.openCamera({
      width: 300,
      height: 400,
      cropping: true,
      compressImageQuality: 0.6,
    })
      .then(image => {
        this.setState({IMG2: image, ImageUri2: image.path});
      })
      .catch(err => {
        console.log(err);
      });
  };
  _openCamera3 = () => {
    ImagePicker.openCamera({
      width: 300,
      height: 400,
      cropping: true,
      compressImageQuality: 0.6,
    })
      .then(image => {
        this.setState({IMG3: image, ImageUri3: image.path});
      })
      .catch(err => {
        console.log(err);
      });
  };
  _openCamera4 = () => {
    ImagePicker.openCamera({
      width: 300,
      height: 400,
      cropping: true,
      compressImageQuality: 0.6,
    })
      .then(image => {
        this.setState({IMG4: image, ImageUri4: image.path});
      })
      .catch(err => {
        console.log(err);
      });
  };

  async getMainCategory() {
    try {
      let formData = new FormData();
      formData = {LanguageId: this.state.lang};
      let response = await fetch(
        this.props.global.baseApiUrl + '/MainCategory',
        {
          method: 'POST',
          body: JSON.stringify(formData),
          headers: {
            'Content-Type': 'application/json',
          },
        },
      );
      let json = await response.json();
      console.log(response);
      if (response.status === 200) {
        this.setState({MainCategory: json});
      }
      console.log(formData);
      console.log(json);
    } catch (error) {
      console.log(error);
      await this.setState({
        loading: false,
      });
    }
  }

  async Edit(image1, image2, image3, image4) {
    console.log(image1);
    console.log(image2);
    console.log(image3);
    console.log(image4);
    this.setState({
      IsModal: true,
      loading: true,
      errorDesc: '',
      hasError: false,
    });
    try {
      const form = new FormData();
      const name1 = image1.path.split('/').reverse()[0];
      const name2 = image2 ? image2.path.split('/').reverse()[0] : null;
      const name3 = image3 ? image3.path.split('/').reverse()[0] : null;
      const name4 = image4 ? image4.path.split('/').reverse()[0] : null;
      form.append('AdsId', this.props.EditData.AdsId);
      form.append('Token', this.props.user.apiToken);
      form.append('Categoryid', JSON.stringify(this.state.selected));
      form.append('Title', this.state.Title);
      form.append('Price', this.state.Price);
      form.append('Percent', this.state.Percent);
      form.append('ExpireTime', this.state.ExpireTime);
      form.append('LanguageId', this.state.lang);
      form.append('Image1', name1);
      image2 ? form.append('Image2', name2) : null;
      image3 ? form.append('Image3', name3) : null;
      image4 ? form.append('Image4', name4) : null;
      form.append('Description', this.state.Description);
      form.append('ImageFilBytes1', {
        uri: image1.path,
        name1,
        type: image1.mime,
      });
      image2
        ? form.append('ImageFilBytes2', {
            uri: image2.path,
            name2,
            type: image2.mime,
          })
        : null;
      image3
        ? form.append('ImageFilBytes3', {
            uri: image3.path,
            name3,
            type: image3.mime,
          })
        : null;
      image4
        ? form.append('ImageFilBytes4', {
            uri: image4.path,
            name4,
            type: image4.mime,
          })
        : null;
      console.log(form);
      let response = await fetch(this.props.global.baseApiUrl + '/EdditAds', {
        method: 'POST',
        body: form,
      });
      console.log(response);
      let json = await response.json();
      //console.log(json);
      if (json === 'SUCCESS') {
        this.setState(
          {
            IsModal: true,
            hasError: true,
            errorDesc: 'Success',
            loading: false,
            Title: '',
            Price: '',
            Percent: '',
            ExpireTime: '',
            Description: '',
            selected: '',
            ImageUri1: '',
            ImageUri2: '',
            ImageUri3: '',
            ImageUri4: '',
            IMG1: '',
            IMG2: '',
            IMG3: '',
            IMG4: '',
            isDateTimePickerVisible: false,
          },
          () => Actions.replace('myRequests'),
        );
      } else {
        this.setState({
          IsModal: true,
          hasError: true,
          errorDesc: 'Error',
          loading: false,
        });
      }
      console.log(form);
    } catch (error) {
      console.log(error);
      this.setState({
        IsModal: true,
        hasError: true,
        errorDesc: 'Error',
        loading: false,
      });
    }
  }
  async getSubCategory() {
    try {
      let formData = new FormData();
      formData = {CateGoryId: this.state.CateGoryId};
      let response = await fetch(
        this.props.global.baseApiUrl + '/SubCategory',
        {
          method: 'POST',
          body: JSON.stringify(formData),
          headers: {
            'Content-Type': 'application/json',
          },
        },
      );
      let json = await response.json();
      if (response.status === 200) {
        this.setState({SubCategory: json});
      }
      console.log(formData);
      console.log(response);
      console.log(json);
    } catch (error) {
      console.log(error);
      await this.setState({
        loading: false,
      });
    }
  }
  render() {
    return (
      <Container style={styles.mainContainer}>
        <StatusBar
          backgroundColor="transparent"
          translucent={true}
          barStyle="dark-content"
        />
        <Content>
          <View style={styles.Content}>
            {this.props.user.role === ('3' || 3) && (
              <Text style={{textAlign: 'center'}}>You Cannt Add Product</Text>
            )}
            {this.props.user.role === ('2' || 2) && (
              <View style={{flex: 1}}>
                <View style={styles.catWrapper}>
                  <Picker
                    note
                    mode="dropdown"
                    style={[styles.TextInput]}
                    selectedValue={this.state.CateGoryId}
                    onValueChange={this.onValueChange.bind(this)}>
                    {this.state.MainCategory.map(v => {
                      return (
                        <Picker.Item
                          key={v.CategoryId}
                          label={v.CategoryName}
                          value={v.CategoryId}
                        />
                      );
                    })}
                  </Picker>
                </View>
                <View style={styles.catWrapper}>
                  <Picker
                    note
                    mode="dropdown"
                    inlineLabel="Category"
                    textStyle={{color: 'red'}}
                    style={[styles.TextInput]}
                    onValueChange={data => this.setState({selected: data})}>
                    {this.state.SubCategory.map(v => {
                      return (
                        <Picker.Item
                          key={v.CategoryId}
                          label={v.CategoryName}
                          value={v.CategoryId}
                        />
                      );
                    })}
                  </Picker>
                </View>
                <TextInput
                  placeholder="Title"
                  style={styles.TextInput}
                  value={this.state.Title}
                  onChangeText={data => this.setState({Title: data})}
                />
                <View style={styles.prices}>
                  <TextInput
                    value={this.state.Price}
                    placeholder="Price"
                    keyboardType="numeric"
                    style={styles.TextInputSpecialR}
                    onChangeText={data => this.setState({Price: data})}
                  />
                  <TextInput
                    value={this.state.Percent}
                    keyboardType="numeric"
                    placeholder="Percent"
                    style={styles.TextInputSpecialL}
                    onChangeText={data => this.setState({Percent: data})}
                  />
                </View>

                <TouchableOpacity
                  style={styles.TextInput}
                  onPress={() =>
                    this.setState({isDateTimePickerVisible: true})
                  }>
                  {this.state.ExpireTime === '' && (
                    <Text style={{paddingVertical: 15, color: '#AAAAAA'}}>
                      Expire Time
                    </Text>
                  )}
                  {this.state.ExpireTime !== '' && (
                    <Text style={{paddingVertical: 15}}>
                      {this.state.ExpireTime}
                    </Text>
                  )}
                </TouchableOpacity>
                <TextInput
                  value={this.state.Description}
                  placeholder="Description"
                  multiline={true}
                  style={[styles.TextInput, {height: 100}]}
                  onChangeText={data => this.setState({Description: data})}
                />
                <View style={styles.Images}>
                  {/* {this.props.Edit && this.ShowImagesEdit1()} */}
                  {this.ShowImages1()}
                  {this.state.IMG1 !== '' && this.ShowImages2()}
                  {this.state.IMG2 !== '' && this.ShowImages3()}
                  {this.state.IMG3 !== '' && this.ShowImages4()}
                </View>
                {this.props.Edit && (
                  <TouchableOpacity
                    onPress={() =>
                      this.Edit(
                        this.state.IMG1,
                        this.state.IMG2,
                        this.state.IMG3,
                        this.state.IMG4,
                      )
                    }>
                    <LinearGradient
                      colors={['#3399ff', '#fff']}
                      style={styles.grButtonGradient}
                      start={{x: 0, y: 1}}
                      end={{x: 1, y: 1}}>
                      <Text style={styles.grButtonText}>Update</Text>
                    </LinearGradient>
                  </TouchableOpacity>
                )}
                {!this.props.Edit && (
                  <TouchableOpacity
                    onPress={() =>
                      this.AddAds(
                        this.state.IMG1,
                        this.state.IMG2,
                        this.state.IMG3,
                        this.state.IMG4,
                      )
                    }>
                    <LinearGradient
                      colors={['#3399ff', '#fff']}
                      style={styles.grButtonGradient}
                      start={{x: 0, y: 1}}
                      end={{x: 1, y: 1}}>
                      <Text style={styles.grButtonText}>Submit</Text>
                    </LinearGradient>
                  </TouchableOpacity>
                )}
              </View>
            )}
          </View>
        </Content>
        <Modal
          onBackdropPress={() => this.setState({IsModal: false})}
          onBackButtonPress={() => this.setState({IsModal: false})}
          isVisible={this.state.IsModal}>
          <View
            style={{
              backgroundColor: '#fff',
              borderRadius: 10,
              height: 200,
              width: '80%',
              alignItems: 'center',
              justifyContent: 'center',
              alignSelf: 'center',
            }}>
            {this.state.loading && (
              <View>
                <ActivityIndicator size="large" color="#0082f0" />
                <Text style={{marginTop: 10}}>Please Wait ...</Text>
              </View>
            )}
            {this.state.hasError && (
              <View>
                <Text style={{marginTop: 10, fontSize: 16}}>
                  {this.state.errorDesc}
                </Text>
              </View>
            )}
          </View>
        </Modal>
        <DateTimePicker
          isVisible={this.state.isDateTimePickerVisible}
          onConfirm={this.handleDatePicked}
          onCancel={this.hideDateTimePicker}
        />
      </Container>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    setUser: user => {
      dispatch(setUser(user));
    },
  };
};

const mapStateToProps = state => {
  return {
    user: state.user,
    global: state.global,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Add);
