import React, {Component} from 'react';
import {View, StatusBar, Dimensions} from 'react-native';

import Tabbar from './Tabbar';
import {connect} from 'react-redux';
import Home from './Main';
import Add from './Add';
import Profile from './Profile';
import Other from './Other';

class IndexScreen extends Component {
  state = {open: false};
  render() {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: 'transparent',
        }}>
        {this.handleChangeIndex()}

        <Tabbar />
      </View>
    );
  }

  handleChangeIndex() {
    if (this.props.index === 0) {
      return <Profile />;
    } else if (this.props.index === 1) {
      return <Other />;
    } else if (this.props.index === 2) {
      return <Home />;
    } else if (this.props.index === 3) {
      return <Add />;
    }
  }
}

const mapStateToProps = state => {
  console.log(state);
  return {
    index: state.Tab.index,
  };
};
export default connect(mapStateToProps)(IndexScreen);
