import React from 'react';
import {Container, Text, Header, Left, Right, Content, Icon} from 'native-base';
import {StyleSheet, TouchableOpacity} from 'react-native';
import {Actions} from 'react-native-router-flux';

class AboutUs extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }
  render() {
    return (
      <Container style={styles.mainContainer}>
        <Header androidStatusBarColor={'#0082f0'} style={styles.Header}>
          <Left>
            <TouchableOpacity onPress={() => Actions.pop()}>
              <Icon style={styles.IconHeader} name="ios-arrow-back" />
            </TouchableOpacity>
          </Left>
          <Right>
            <Text style={styles.TextHeader}>About Us</Text>
          </Right>
        </Header>
        <Content style={styles.ContentStyle}>
          <Text>AboutUs</Text>
        </Content>
      </Container>
    );
  }
}

export default AboutUs;

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#0082f0',
  },
  Header: {
    backgroundColor: '#0082f0',
    elevation: 0,
    marginTop: 20,
  },
  TextHeader: {
    color: 'white',
    marginRight: 15,
    fontWeight: 'bold',
    fontSize: 18,
  },
  IconHeader: {
    marginLeft: 15,
    color: 'white',
  },
  ContentStyle: {
    flex: 1,
    backgroundColor: '#fff',
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    paddingTop: 20,
  },
});
