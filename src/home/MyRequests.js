import React from 'react';
import {
  Text,
  TouchableOpacity,
  ActivityIndicator,
  FlatList,
  View,
  Image,
} from 'react-native';
import {Content, Container, Header, Right, Left, Icon} from 'native-base';
import styles from '../assets/styles/MyRequestsStyle';
import {setUser} from '../redux/Actions/Index';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import Modal from 'react-native-modal';
class MyRequest extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      lang: 1,
      loading: true,
      List: [],
      options: false,
      optionItem: '',
      IsModal: false,
      hasError: false,
      errorDesc: 'Success',
      loadingModal: false,
    };
  }
  componentDidMount() {
    this.GetList();
  }
  async GetList() {
    try {
      let formData = new FormData();
      formData = {
        Token: this.props.user.apiToken,
      };
      let response = await fetch(
        this.props.global.baseApiUrl + '/MyAdsSeller',
        {
          method: 'POST',
          body: JSON.stringify(formData),
          headers: {
            'Content-Type': 'application/json',
          },
        },
      );
      console.log(formData);
      console.log(response);

      let json = await response.json();
      console.log(json);

      this.setState({
        loading: false,
        List: json,
      });
    } catch (error) {
      this.setState({
        loading: false,
      });
    }
  }
  ListEmptyComment() {
    if (this.state.loading) {
      return (
        <View>
          <ActivityIndicator color="#0082f0" size="large" />
        </View>
      );
    } else {
      return (
        <View style={styles.Empty}>
          <Text style={styles.TextEmpty}>Empty !</Text>
        </View>
      );
    }
  }
  async Remove(item) {
    this.setState({
      loadingModal: true,
      IsModal: true,
      errorDesc: '',
      hasError: false,
    });
    try {
      let formData = new FormData();
      formData = {
        Token: this.props.user.apiToken,
        AdsId: item,
      };
      let response = await fetch(this.props.global.baseApiUrl + '/DeletAds', {
        method: 'POST',
        body: JSON.stringify(formData),
        headers: {
          'Content-Type': 'application/json',
        },
      });
      console.log(formData);
      console.log(response);

      let json = await response.json();
      console.log(json);

      this.setState(
        {
          loadingModal: false,
          IsModal: true,
          errorDesc: 'SUCCESS',
          hasError: true,
        },
        () => this.GetList(),
      );
    } catch (error) {
      this.setState({
        loading: false,
        IsModal: true,
        errorDesc: 'FAILED',
        hasError: true,
      });
    }
  }
  renderItem({item}) {
    console.log(item);
    return (
      <TouchableOpacity
        activeOpacity={1}
        onPress={() => this.setState({options: false})}
        style={styles.WrapperList}>
        <TouchableOpacity
          onPress={() => {
            !this.state.options
              ? this.setState({options: true, optionItem: item.AdsId})
              : this.setState({options: false, optionItem: ''});
          }}
          style={styles.Optionbtn}>
          <Icon style={styles.OptionIcon} name="more" />
        </TouchableOpacity>
        {this.state.options && this.state.optionItem === item.AdsId && (
          <View style={styles.Items}>
            <TouchableOpacity
              onPress={() => this.Remove(item.AdsId)}
              activeOpacity={0.9}
              style={styles.Item}>
              <Text>Remove</Text>
              <Icon style={styles.TrashIcon} name="trash" />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => Actions.push('add', {EditData: item, Edit: true})}
              activeOpacity={0.9}
              style={styles.Item}>
              <Text>Edit</Text>
              <Icon style={styles.EditIcon} name="create" />
            </TouchableOpacity>
          </View>
        )}
        <Image
          source={{
            uri: 'http://luxuryglass.ir/Images/Ads/Img/' + item.MainImg,
          }}
          style={styles.ImgList}
        />
        <View style={styles.ListData}>
          <View style={styles.WrapperTitle}>
            <Text style={styles.Title}>{item.Title}</Text>
            <Text style={styles.OffText}>{item.Percent + '%'}</Text>
          </View>
          <View style={styles.WrapperPrice}>
            <View style={styles.Price}>
              <Text style={styles.PriceNew}>{item.NewPrice + ' $'}</Text>
              <Text style={styles.PriceOld}>{item.OldPrice + ' $'}</Text>
            </View>
            <View style={styles.BuyCount}>
              <Text style={styles.BuyCountText}>{item.Countbuy + ' Buy'}</Text>
              <Icon style={styles.BuyCountIcon} name="cart" />
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
  render() {
    return (
      <Container style={styles.mainContainer}>
        <Header androidStatusBarColor={'#0082f0'} style={styles.Header}>
          <Left>
            <TouchableOpacity onPress={() => Actions.pop()}>
              <Icon style={styles.IconHeader} name="ios-arrow-back" />
            </TouchableOpacity>
          </Left>
          <Right>
            <Text style={styles.TextHeader}>My Requests</Text>
          </Right>
        </Header>
        <Content style={styles.ContentStyle}>
          <FlatList
            data={this.state.List}
            style={styles.flatlist}
            renderItem={this.renderItem.bind(this)}
            keyExtractor={item => JSON.stringify(item.AdsId)}
            ListEmptyComponent={this.ListEmptyComment.bind(this)}
          />
        </Content>
        <Modal
          onBackdropPress={() => this.setState({IsModal: false})}
          onBackButtonPress={() => this.setState({IsModal: false})}
          isVisible={this.state.IsModal}>
          <View
            style={{
              backgroundColor: '#fff',
              borderRadius: 10,
              height: 200,
              width: '80%',
              alignItems: 'center',
              justifyContent: 'center',
              alignSelf: 'center',
            }}>
            {this.state.loadingModal && (
              <View>
                <ActivityIndicator size="large" color="#0082f0" />
                <Text style={{marginTop: 10}}>Please Wait ...</Text>
              </View>
            )}
            {this.state.hasError && (
              <View>
                <Text style={{marginTop: 10, fontSize: 16}}>
                  {this.state.errorDesc}
                </Text>
              </View>
            )}
          </View>
        </Modal>
      </Container>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    setUser: user => {
      dispatch(setUser(user));
    },
  };
};

const mapStateToProps = state => {
  return {
    user: state.user,
    global: state.global,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MyRequest);
