import React from 'react';
import {StatusBar, Text, View, Image, TouchableOpacity} from 'react-native';
import {Container, Icon} from 'native-base';
import styles from '../assets/styles/ProfileStyle';
import {setUser} from '../redux/Actions/Index';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
class Other extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <Container style={styles.mainContainer}>
        <StatusBar
          backgroundColor="transparent"
          translucent={true}
          barStyle="dark-content"
        />
        <View style={styles.content}>
          <Text>other</Text>
        </View>
      </Container>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    setUser: user => {
      dispatch(setUser(user));
    },
  };
};

const mapStateToProps = state => {
  return {
    user: state.user,
    global: state.global,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Other);
