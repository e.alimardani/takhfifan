import React from 'react';
import {
  Text,
  TouchableOpacity,
  ActivityIndicator,
  FlatList,
  View,
  Image,
  Dimensions,
  TextInput,
  StatusBar,
  Linking,
} from 'react-native';
import {Content, Container, Header, Right, Left, Icon} from 'native-base';
import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';
import styles from '../assets/styles/DetailStyle';
import {setUser} from '../redux/Actions/Index';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
import Carousel from 'react-native-snap-carousel';
import Modal from 'react-native-modal';
const width = Dimensions.get('window').width;
class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      DetailData: [],
      DetailPhoto: [],
      loadingSlide: true,
      Comments: [],
      PageComments: 1,
      loadingComments: true,
      TextComment: '',
      Count: 1,
      IsModal: false,
      Loading: false,
      hasError: false,
      errorDesc: '',
    };
  }
  componentDidMount() {
    this.GetDetail();
    this.GetPhoto();
    this.GetComment();
  }
  renderHeader({item}) {
    return (
      <TouchableOpacity
        onPress={() => Actions.push('catList', {Data: item})}
        style={styles.renderHeader}>
        <Image
          source={{
            uri: 'http://luxuryglass.ir/Images/Ads/Img/' + item.Image,
          }}
          resizeMode={'cover'}
          style={styles.ImageHeader}
        />
        <Text style={styles.TextRender}>{item.CategoryName}</Text>
      </TouchableOpacity>
    );
  }

  async GetPhoto() {
    try {
      let formData = new FormData();
      formData = {AdsId: this.props.Data.AdsId};
      let response = await fetch(
        this.props.global.baseApiUrl + '/GalleryAdsDetails',
        {
          method: 'POST',
          body: JSON.stringify(formData),
          headers: {
            'Content-Type': 'application/json',
          },
        },
      );
      let json = await response.json();
      console.log(json);
      console.log(formData);
      this.setState({
        loadingSlide: false,
        DetailPhoto: json,
      });
    } catch (error) {
      this.setState({
        loadingSlide: false,
      });
    }
  }

  async GetComment() {
    try {
      let formData = new FormData();
      formData = {
        AdsId: this.props.Data.AdsId,
        Page: this.state.PageComments,
        Count: 4,
      };
      let response = await fetch(
        this.props.global.baseApiUrl + '/ShowComment',
        {
          method: 'POST',
          body: JSON.stringify(formData),
          headers: {
            'Content-Type': 'application/json',
          },
        },
      );
      let json = await response.json();
      console.log('sadjfnasdfjnasdkjfnasdlkjnf');
      console.log(json);
      console.log(formData);
      this.setState({
        loadingComments: false,
        Comments: json,
      });
    } catch (error) {
      this.setState({
        loadingComments: false,
      });
    }
  }

  async AddComment() {
    this.setState({
      IsModal: true,
      Loading: true,
      hasError: false,
      errorDesc: '',
    });
    try {
      let formData = new FormData();
      formData = {
        AdsId: this.props.Data.AdsId,
        Token: this.props.user.apiToken,
        Text: this.state.TextComment,
      };
      let response = await fetch(this.props.global.baseApiUrl + '/Comment', {
        method: 'POST',
        body: JSON.stringify(formData),
        headers: {
          'Content-Type': 'application/json',
        },
      });
      let json = await response.json();
      console.log(json);
      console.log(formData);

      if (json === 'SUCCESS') {
        this.setState({
          IsModal: true,
          Loading: false,
          hasError: true,
          errorDesc: 'SUCCESS',
          TextComment: '',
        });
      } else {
        this.setState({
          IsModal: true,
          Loading: false,
          hasError: true,
          errorDesc: 'FAILED',
        });
      }
    } catch (error) {
      this.setState({
        IsModal: true,
        Loading: false,
        hasError: true,
        errorDesc: 'FAILED',
      });
    }
  }

  async Buy() {
    this.setState({Loading: true, IsModal: true, hasError: false});
    try {
      let formData = new FormData();
      formData = {
        AdsId: this.props.Data.AdsId,
        Token: this.props.user.apiToken,
        Count: this.state.Count,
      };
      let response = await fetch(this.props.global.baseApiUrl + '/Order', {
        method: 'POST',
        body: JSON.stringify(formData),
        headers: {
          'Content-Type': 'application/json',
        },
      });
      let json = await response.json();
      console.log(json);
      console.log(formData);
      if (json === 'Catch') {
        this.setState({
          Loading: false,
          IsModal: true,
          hasError: true,
          errorDesc: 'Error Please Try Again Later',
        });
      } else {
        this.setState({
          Loading: false,
          IsModal: true,
          hasError: true,
          errorDesc: 'Success',
          BuyModal: false,
        });
      }
    } catch (error) {
      this.setState({
        Loading: false,
        IsModal: true,
        hasError: true,
        errorDesc: 'Error Please Try Again Later',
      });
    }
  }
  renderComment({item}) {
    return (
      <View style={styles.Comments}>
        <View style={styles.CommentPerson}>
          <View style={styles.WrapperPerson}>
            <Text style={styles.personText}>{item.UserName}</Text>
            <Icon style={styles.personIcon} name="person" />
          </View>
          <Text style={styles.CommentDate}>{item.DateTime}</Text>
        </View>
        <Text style={styles.DscComment}>{item.Text}</Text>
      </View>
    );
  }

  async GetDetail() {
    try {
      let formData = new FormData();
      formData = {AdsId: this.props.Data.AdsId};
      let response = await fetch(this.props.global.baseApiUrl + '/AdsDetails', {
        method: 'POST',
        body: JSON.stringify(formData),
        headers: {
          'Content-Type': 'application/json',
        },
      });
      let json = await response.json();
      console.log(json);
      console.log(formData);
      this.setState({
        loading: false,
        DetailData: json,
      });
    } catch (error) {
      this.setState({
        loading: false,
      });
    }
  }
  _renderItem({item}) {
    return (
      <TouchableOpacity style={styles.carouselItem}>
        <Image
          source={{
            uri: 'http://luxuryglass.ir/Images/Ads/Img/' + item.Image,
          }}
          style={styles.ImageCarousel}
        />
      </TouchableOpacity>
    );
  }
  ListEmptyComment() {
    if (this.state.loadingComments) {
      return (
        <View>
          <ActivityIndicator color="#0082f0" size="large" />
        </View>
      );
    } else {
      return (
        <View style={styles.Empty}>
          <Text style={styles.TextEmpty}>No comments !</Text>
        </View>
      );
    }
  }
  openMap() {
    console.log(' directions');
    Linking.openURL(
      `http://maps.apple.com/maps?daddr=${parseFloat(
        this.state.DetailData.Lat,
      )},${parseFloat(this.state.DetailData.Long)}`,
    );
  }
  render() {
    return (
      <Container style={styles.mainContainer}>
        <Header androidStatusBarColor={'#0082f0'} style={styles.Header}>
          <Left>
            <TouchableOpacity onPress={() => Actions.pop()}>
              <Icon style={styles.IconHeader} name="ios-arrow-back" />
            </TouchableOpacity>
          </Left>
          <Right>
            <Text style={styles.TextHeader}>Detail</Text>
          </Right>
        </Header>
        <View style={styles.HeaderWrapper}>
          {this.state.loadingHeader && (
            <ActivityIndicator size="large" color="#0082f0" />
          )}
          {!this.state.loadingHeader && (
            <FlatList
              showsHorizontalScrollIndicator={false}
              data={this.props.DataHeader}
              renderItem={this.renderHeader.bind(this)}
              keyExtractor={item => JSON.stringify(item.CategoryId)}
              horizontal={true}
              style={styles.FlatList}
            />
          )}
        </View>
        <Content style={styles.ContentStyle}>
          <View style={styles.Carousel}>
            {this.state.loadingSlide && (
              <ActivityIndicator size="large" color="#0082f0" />
            )}
            {!this.state.loadingSlide && (
              <Carousel
                ref={c => {
                  this._carousel = c;
                }}
                data={this.state.DetailPhoto}
                renderItem={this._renderItem.bind(this)}
                sliderWidth={width - 30}
                itemWidth={width - 30}
                loop={true}
              />
            )}
          </View>
          {this.state.DetailData !== [] && (
            <View>
              <View style={styles.AdrData}>
                <View style={styles.AdrNames}>
                  <Text style={styles.AdrName}>
                    {this.state.DetailData.Title}
                  </Text>
                  <View style={styles.AdrLocation}>
                    <Text style={styles.AdrOrigin}>
                      {this.state.DetailData.Address}
                    </Text>
                    <Icon style={styles.AdrIcon} name="pin" />
                  </View>
                </View>
                <View style={styles.AdrDsc}>
                  <Text style={styles.AdrTextDsc}>
                    {this.state.DetailData.Description}
                  </Text>
                </View>
              </View>
              <View style={styles.CostData}>
                <View style={styles.BuyData}>
                  <Icon style={styles.BuyDataIcon} name="cart" />
                  <Text style={styles.BuyDataText}>
                    {this.state.DetailData.CountBuy + ' Buy'}
                  </Text>
                </View>
                <View style={styles.OffData}>
                  <Text style={styles.OffDataIcon} name="pin">
                    %
                  </Text>
                  <Text style={styles.OffDataText}>
                    {this.state.DetailData.Percent + '% Discont'}
                  </Text>
                </View>
                <View style={styles.PriceData}>
                  <Text style={styles.PriceDataText}>
                    {this.state.DetailData.LastPrice + ' $'}
                  </Text>
                  <Text style={styles.PriceDataOffText}>
                    {this.state.DetailData.Price + ' $'}
                  </Text>
                </View>
              </View>
              <View style={styles.DescriptionSellerData}>
                <View style={styles.DescriptionSellerTitle}>
                  <Text style={styles.DescriptionSellerDataText}>
                    {'Description Seller ' + this.state.DetailData.Title}
                  </Text>
                  <View style={styles.DescriptionSellerIcon} />
                </View>
                <Text style={styles.DescriptionSellerText}>
                  {this.state.DetailData.DescriptionSeller}
                </Text>
              </View>
              <View style={styles.TimeDate}>
                <View style={styles.UseTime}>
                  <View style={styles.TimeTextWrapper}>
                    <Icon style={styles.TimeIcon} name="calendar" />
                    <Text style={styles.TimeText}>time date :</Text>
                  </View>
                  <Text style={styles.DateData}>
                    {this.state.DetailData.DateTime}
                  </Text>
                </View>
                <View style={styles.UseTime}>
                  <View style={styles.TimeTextWrapper}>
                    <Icon style={styles.TimeIcon} name="time" />
                    <Text style={styles.TimeText}>time date :</Text>
                  </View>
                  <Text style={styles.DateData}>
                    {this.state.DetailData.ExpireTime}
                  </Text>
                </View>
                <View style={styles.UseTime}>
                  <View style={styles.TimeTextWrapper}>
                    <Icon style={styles.TimeIcon} name="call" />
                    <Text style={styles.TimeText}>
                      Phone :
                      <Text style={styles.TextPhone}>
                        {' ' + this.state.DetailData.Phone}
                      </Text>
                    </Text>
                  </View>
                </View>
              </View>
              {this.state.DetailData.Lat && (
                <MapView
                  provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                  style={styles.map}
                  onPress={() => this.openMap()}
                  region={{
                    latitude: parseFloat(this.state.DetailData.Lat),
                    longitude: parseFloat(this.state.DetailData.Long),
                    latitudeDelta: 0.015,
                    longitudeDelta: 0.0121,
                  }}>
                  <Marker
                    coordinate={{
                      latitude: parseFloat(this.state.DetailData.Lat),
                      longitude: parseFloat(this.state.DetailData.Long),
                    }}
                  />
                </MapView>
              )}
              <FlatList
                data={this.state.Comments}
                renderItem={this.renderComment.bind(this)}
                keyExtractor={item => JSON.stringify(item.Id)}
                style={styles.FlatList}
                ListEmptyComponent={this.ListEmptyComment.bind(this)}
              />
              {!this.state.loadingComments && this.state.Comments === [] && (
                <TouchableOpacity style={styles.ShowMore}>
                  <Text style={styles.ShowMoreText}>Show More Comments</Text>
                </TouchableOpacity>
              )}
              <View style={styles.AddComment}>
                <View style={styles.AddCommentTitle}>
                  <Text style={styles.CommentTitleText}>Add Comment</Text>
                  <Icon style={styles.AddcommentIcon} name="chatboxes" />
                </View>
                <View style={styles.InputWrapper}>
                  <View style={styles.InputItem}>
                    <Text>Description : </Text>
                    <TextInput
                      onChangeText={text => this.setState({TextComment: text})}
                      value={this.state.TextComment}
                      multiline={true}
                      style={styles.InputTextDsc}
                      placeholder="write something ..."
                    />
                  </View>
                  <TouchableOpacity
                    onPress={() => this.AddComment()}
                    style={styles.SubmitComment}>
                    <Text style={styles.ShowMoreText}>Submit</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          )}
        </Content>
        <Modal
          onBackdropPress={() => this.setState({BuyModal: false})}
          onBackButtonPress={() => this.setState({BuyModal: false})}
          animationIn="bounceInDown"
          animationOut="bounceOutDown"
          style={styles.modalBuyWrapper}
          isVisible={this.state.BuyModal}>
          <View style={styles.modalView}>
            <View style={styles.Countnum}>
              <Text>Choose Number Of Product :</Text>
              <View style={styles.CountWrapper}>
                <TouchableOpacity
                  onPress={() => {
                    this.state.Count < 10
                      ? this.setState({Count: this.state.Count + 1})
                      : null;
                  }}
                  style={styles.CountUp}>
                  <Text style={styles.CountUpText}>+</Text>
                </TouchableOpacity>
                <View style={styles.CountView}>
                  <Text>{this.state.Count}</Text>
                </View>
                <TouchableOpacity
                  onPress={() => {
                    this.state.Count > 1
                      ? this.setState({Count: this.state.Count - 1})
                      : null;
                  }}
                  style={styles.CountUp}>
                  <Text style={styles.CountUpText}>-</Text>
                </TouchableOpacity>
              </View>
            </View>
            <TouchableOpacity
              onPress={() => this.Buy()}
              style={styles.AcceptBtn}>
              <Text style={styles.AcceptBtnText}>Accept</Text>
            </TouchableOpacity>
          </View>
        </Modal>
        <Modal
          onBackdropPress={() => this.setState({IsModal: false})}
          onBackButtonPress={() => this.setState({IsModal: false})}
          animationIn="fadeIn"
          animationOut="fadeOut"
          isVisible={this.state.IsModal}>
          <View style={styles.ModalError}>
            {this.state.Loading && (
              <View>
                <ActivityIndicator size="large" color="#0082f0" />
                <Text style={styles.waitText}>Please Wait ...</Text>
              </View>
            )}
            {this.state.hasError && (
              <View>
                <Text style={styles.TextDsc}>{this.state.errorDesc}</Text>
              </View>
            )}
          </View>
        </Modal>
        <TouchableOpacity
          onPress={() => this.setState({BuyModal: true})}
          activeOpacity={0.5}
          style={styles.fixBtn}>
          <Text style={styles.fixBtnText}>BUY</Text>
          <Icon style={styles.fixBtnIcon} name="cart" />
        </TouchableOpacity>
      </Container>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    setUser: user => {
      dispatch(setUser(user));
    },
  };
};

const mapStateToProps = state => {
  return {
    user: state.user,
    global: state.global,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Profile);
