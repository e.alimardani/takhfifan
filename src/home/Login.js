import React from 'react';
import {
  StatusBar,
  TextInput,
  TouchableOpacity,
  ImageBackground,
  ActivityIndicator,
} from 'react-native';
import {Text, View, Container, Header, Body, Image} from 'native-base';
import styles from '../assets/styles/LoginStyle';
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';
import LinearGradient from 'react-native-linear-gradient';
import Modal from 'react-native-modal';
class Login extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      Name: '',
      Email: '',
      lang: 1,
      IsModal: false,
      hasError: false,
      Loading: false,
      errorDesc: '',
    };
    this.loginRequest = this.loginRequest.bind(this);
  }

  async loginRequest() {
    this.setState({
      IsModal: true,
      Loading: true,
      hasError: false,
      errorDesc: '',
    });
    try {
      let formData = new FormData();
      formData = {
        Name: this.state.Name,
        Email: this.state.Email,
        DeviceId: '111',
        LanguageId: this.state.lang,
      };
      console.log(formData);
      let response = await fetch(
        this.props.global.baseApiUrl + '/RegisterUser',
        {
          method: 'POST',
          body: JSON.stringify(formData),
          headers: {
            'Content-Type': 'application/json',
          },
        },
      );
      console.log(response);

      if (response.status === 200) {
        this.setState({
          IsModal: false,
          Loading: false,
          hasError: false,
          errorDesc: '',
        });
        Actions.push('addCode', {Email: this.state.Email});
      } else {
        this.setState({
          IsModal: true,
          Loading: false,
          hasError: true,
          errorDesc: 'FAILED',
        });
      }
    } catch {
      this.setState({
        IsModal: true,
        Loading: false,
        hasError: true,
        errorDesc: 'FAILED',
      });
    }
  }

  render() {
    return (
      <Container style={styles.mainContainer}>
        <StatusBar
          backgroundColor="transparent"
          barStyle="dark-content"
          translucent={true}
        />
        <View
          style={{
            width: '100%',
            height: '100%',
            backgroundColor: '#fff',
            opacity: 0.4,
            flex: 1,
            position: 'absolute',
          }}
        />
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
          }}>
          <View style={styles.slide}>
            <View style={styles.inputContainer}>
              <Text style={styles.inputLabel}>Name</Text>
              <TextInput
                underlineColorAndroid="rgba(0,0,0,0)"
                style={styles.inputMain}
                keyboardType="default"
                value={this.state.Name}
                onChangeText={data => this.setState({Name: data})}
              />
            </View>
            <View style={styles.inputContainer}>
              <Text style={styles.inputLabel}>Email</Text>
              <TextInput
                value={this.state.Email}
                underlineColorAndroid="rgba(0,0,0,0)"
                style={styles.inputMain}
                onChangeText={data => this.setState({Email: data})}
              />
            </View>
            <TouchableOpacity
              style={styles.colorGrButton}
              activeOpacity={0.8}
              onPress={this.loginRequest}>
              <LinearGradient
                colors={['#3399ff', '#fff']}
                style={styles.grButtonGradient}
                start={{x: 0, y: 1}}
                end={{x: 1, y: 1}}>
                <Text style={styles.grButtonText}>Login</Text>
              </LinearGradient>
            </TouchableOpacity>
          </View>
        </View>
        <Modal
          onBackdropPress={() => this.setState({IsModal: false})}
          onBackButtonPress={() => this.setState({IsModal: false})}
          animationIn="fadeIn"
          animationOut="fadeOut"
          isVisible={this.state.IsModal}>
          <View style={styles.ModalError}>
            {this.state.Loading && (
              <View>
                <ActivityIndicator size="large" color="#0082f0" />
                <Text style={styles.waitText}>Please Wait ...</Text>
              </View>
            )}
            {this.state.hasError && (
              <View>
                <Text style={styles.TextDsc}>{this.state.errorDesc}</Text>
              </View>
            )}
          </View>
        </Modal>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.user,
    global: state.global,
  };
};

export default connect(
  mapStateToProps,
  null,
)(Login);
