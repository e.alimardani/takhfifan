import * as React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Dimensions,
  View,
  Animated,
} from 'react-native';
import * as shape from 'd3-shape';
import {Svg, Path} from 'react-native-svg';
import StaticTabbar from './StaticTabbar';
import {connect} from 'react-redux';

const AnimatedSvg = Animated.createAnimatedComponent(Svg);
const {width} = Dimensions.get('window');
const height = 58;
const tabs = true
  ? [
      {
        name: 'person',
      },
      {
        name: 'information-circle',
      },
      {
        name: 'home',
      },
      {
        name: 'add',
      },
    ]
  : [
      {
        name: 'person',
      },
      {
        name: 'information-circle',
      },
      {
        name: 'home',
      },
    ];
const tabWidth = width / tabs.length;
const backgroundColor = '#0082f0';

const getPath = (): string => {
  const left = shape
    .line()
    .x(d => d.x)
    .y(d => d.y)([{x: 0, y: 0}, {x: width, y: 0}]);
  const tab = shape
    .line()
    .x(d => d.x)
    .y(d => d.y)
    .curve(shape.curveBasis)([
    {x: width, y: 0},
    {x: width - 10, y: 0},
    {x: width + 8, y: 5},
    {x: width + 18, y: height - 20},
    {x: width + tabWidth - 18, y: height - 20},
    {x: width + tabWidth - 8, y: 5},
    {x: width + tabWidth + 10, y: 0},
    {x: width + tabWidth, y: 0},
  ]);
  const right = shape
    .line()
    .x(d => d.x)
    .y(d => d.y)([
    {x: width + tabWidth, y: 0},
    {x: width * 2, y: 0},
    {x: width * 2, y: height},
    {x: 0, y: height},
    {x: 0, y: 0},
  ]);
  return `${left} ${tab} ${right}`;
};
const d = getPath();

interface TabbarProps {}

// eslint-disable-next-line react/prefer-stateless-function
class Tabbar extends React.PureComponent<TabbarProps> {
  value = new Animated.Value((2 * width) / tabs.length);

  render() {
    const {value} = this;
    const translateX = value.interpolate({
      inputRange: [0, width],
      outputRange: [-width, 0],
    });
    return (
      <View
        style={{
          backgroundColor: 'transparent',
          position: 'absolute',
          bottom: 0,
          right: 0,
          left: 0,
        }}>
        <View style={{backgroundColor: 'transparent'}} {...{height, width}}>
          <AnimatedSvg
            width={width * 2.1}
            {...{height}}
            style={{transform: [{translateX}], backgroundColor: 'transparent'}}>
            <Path fill={backgroundColor} {...{d}} />
          </AnimatedSvg>
          <View style={StyleSheet.absoluteFill}>
            <StaticTabbar {...{tabs, value}} />
          </View>
        </View>
        <SafeAreaView style={styles.container} />
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.user,
    global: state.global,
  };
};

export default connect(
  mapStateToProps,
  null,
)(Tabbar);

const styles = StyleSheet.create({
  container: {
    backgroundColor,
  },
});
