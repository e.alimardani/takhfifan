import React, {Component} from 'react';
import {View, Container, Text, Icon} from 'native-base';
import {StyleSheet, Image, TouchableOpacity, Platform} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';
class MainDrawer extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }
  LogOut() {
    AsyncStorage.removeItem('UserToken');
    Actions.replace('splash');
  }
  render() {
    return (
      <Container style={styles.Container}>
        <View style={styles.Content}>
          <TouchableOpacity
            onPress={() => Actions.push('aboutUs')}
            style={styles.Items}>
            <Icon style={styles.IconItem} name="ios-bookmark" />
            <Text style={styles.TextItem}>About Us</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => Actions.push('contactUs')}
            style={styles.Items}>
            <Icon style={styles.IconItem} name="call" />
            <Text style={styles.TextItem}>Contact US</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => Actions.push('rules')}
            style={styles.Items}>
            <Icon style={styles.IconItem} name="ios-filing" />
            <Text style={styles.TextItem}>Rules</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.LogOut()} style={styles.Items}>
            <Icon style={styles.IconItem} name="ios-log-out" />
            <Text style={styles.TextItem}>Log Out</Text>
          </TouchableOpacity>
        </View>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.user,
    global: state.global,
  };
};

export default connect(
  mapStateToProps,
  null,
)(MainDrawer);

const styles = StyleSheet.create({
  ImgStyle: {
    width: 100,
    height: 100,
    borderRadius: 10,
    alignSelf: 'flex-end',
  },
  Container: {
    backgroundColor: '#F8F8F8',
    justifyContent: 'center',
  },
  Content: {
    padding: 15,
  },
  nametext: {
    ...Platform.select({
      ios: {
        fontFamily: 'IRANSansMobile',
        fontWeight: 'bold',
      },
      android: {
        fontFamily: 'IRANSansMobile_Bold',
      },
    }),
    textAlign: 'right',
    fontSize: 18,
    marginTop: 15,
    marginBottom: -5,
  },
  pricetext: {
    ...Platform.select({
      ios: {
        fontFamily: 'IRANSansMobile',
        fontWeight: 'bold',
      },
      android: {
        fontFamily: 'IRANSansMobile_Bold',
      },
    }),
    textAlign: 'right',
    color: '#67199a',
    marginBottom: 40,
  },
  Items: {
    alignItems: 'center',
    flexDirection: 'row',
    borderBottomColor: '#eaeaea',
    borderBottomWidth: 1,
    paddingVertical: 10,
  },
  IconItem: {
    color: '#0082f0',
    fontSize: 26,
  },
  TextItem: {
    ...Platform.select({
      ios: {
        fontFamily: 'IRANSansMobile',
        fontWeight: 'bold',
      },
      android: {
        fontFamily: 'IRANSansMobile_Bold',
      },
    }),
    textAlign: 'right',
    marginLeft: 10,
    color: '#7C7F80',
  },
  ActiveTextItem: {
    ...Platform.select({
      ios: {
        fontFamily: 'IRANSansMobile',
        fontWeight: 'bold',
      },
      android: {
        fontFamily: 'IRANSansMobile_Bold',
      },
    }),
    textAlign: 'right',
    marginRight: 10,
    color: '#67199a',
  },
  ActiveIconItem: {
    color: '#67199a',
    fontSize: 26,
  },
});
