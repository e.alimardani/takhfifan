import React from 'react';
import {
  ActivityIndicator,
  View,
  FlatList,
  TouchableOpacity,
  Image,
  Dimensions,
  StatusBar,
} from 'react-native';
import {Text, Icon, Content, Container, Header, Left, Right} from 'native-base';
import styles from '../assets/styles/MainStyle';
import {setUser} from '../redux/Actions/Index';
import {connect} from 'react-redux';
import Carousel from 'react-native-snap-carousel';
import {Actions} from 'react-native-router-flux';

const width = Dimensions.get('window').width;

class Main extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      Categories: [],
      loadingHeader: true,
      Lang: '1',
      loadingTodayAds: true,
      TodayAds: [],
      loadingMostOffer: true,
      MostOffer: [],
      BestAds: [],
      loadingBestAds: true,
    };
  }
  componentDidMount() {
    this.GetHeader();
  }
  async GetTodayAds() {
    try {
      let formData = new FormData();
      formData = {
        LanguageId: this.state.Lang,
        Page: '1',
        Count: '10',
      };
      let response = await fetch(this.props.global.baseApiUrl + '/TodayAds', {
        method: 'POST',
        body: JSON.stringify(formData),
        headers: {
          'Content-Type': 'application/json',
        },
      });
      let json = await response.json();
      this.GetMostOffer();
      this.setState({
        loadingTodayAds: false,
        TodayAds: json,
      });
    } catch (error) {
      this.setState({
        loadingTodayAds: false,
      });
    }
  }
  async GetMostOffer() {
    try {
      let formData = new FormData();
      formData = {
        LanguageId: this.state.Lang,
        Page: '1',
        Count: '10',
      };
      let response = await fetch(
        this.props.global.baseApiUrl + '/MorePercent',
        {
          method: 'POST',
          body: JSON.stringify(formData),
          headers: {
            'Content-Type': 'application/json',
          },
        },
      );
      let json = await response.json();
      this.GetBest();
      this.setState({
        loadingMostOffer: false,
        MostOffer: json,
      });
    } catch (error) {
      this.setState({
        loadingMostOffer: false,
      });
    }
  }
  async GetBest() {
    try {
      let formData = new FormData();
      formData = {
        LanguageId: this.state.Lang,
        Page: '1',
        Count: '10',
      };
      let response = await fetch(this.props.global.baseApiUrl + '/BestAds', {
        method: 'POST',
        body: JSON.stringify(formData),
        headers: {
          'Content-Type': 'application/json',
        },
      });
      let json = await response.json();
      console.log(formData);
      console.log(json);
      this.setState({
        loadingBestAds: false,
        BestAds: json,
      });
    } catch (error) {
      this.setState({
        loadingBestAds: false,
      });
    }
  }
  async GetHeader() {
    try {
      let formData = new FormData();
      formData = {
        LanguageId: this.state.Lang,
      };
      let response = await fetch(
        this.props.global.baseApiUrl + '/SubCategoryForMainPage',
        {
          method: 'POST',
          body: JSON.stringify(formData),
          headers: {
            'Content-Type': 'application/json',
          },
        },
      );
      let json = await response.json();
      this.GetTodayAds();
      this.setState({
        loadingHeader: false,
        Categories: json,
      });
    } catch (error) {
      this.setState({
        loadingHeader: false,
      });
    }
  }
  renderHeader({item}) {
    return (
      <TouchableOpacity
        onPress={() =>
          Actions.push('catList', {
            Data: item,
            DataHeader: this.state.Categories,
          })
        }
        style={styles.renderHeader}>
        <Image
          source={{
            uri: 'http://luxuryglass.ir/Images/Ads/Img/' + item.Image,
          }}
          resizeMode={'cover'}
          style={styles.ImageHeader}
        />
        <Text style={styles.TextRender}>{item.CategoryName}</Text>
      </TouchableOpacity>
    );
  }
  _renderItem({item}) {
    return (
      <TouchableOpacity
        onPress={() =>
          Actions.push('detail', {
            Data: item,
            DataHeader: this.state.Categories,
          })
        }
        style={styles.carouselItem}>
        <Image
          source={{
            uri: 'http://luxuryglass.ir/Images/Ads/Img/' + item.MainImg,
          }}
          style={styles.ImageCarousel}
        />
        <Text style={styles.TempTextOff}>{item.Percent}%</Text>
        <View style={styles.TempViewOff} />
        <View style={styles.WrapperRow}>
          <View style={styles.TempView} />
          <View style={styles.TopRowCart}>
            <Text numberOfLines={1} style={styles.TextTitle}>
              {item.Title}
            </Text>
            <Text style={styles.textCenterPrice}>{item.NewPrice}$</Text>
          </View>
          <View style={styles.BottomRowCart}>
            <Text style={styles.OffText}>{item.OldPrice}</Text>
            <View style={styles.WrapperCartIcon}>
              <Icon style={styles.CartIcon} name="cart" />
              <Text style={styles.textCenter}>{item.Countbuy}</Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
  render() {
    return (
      <Container style={styles.mainContainer}>
        <StatusBar
          backgroundColor="transparent"
          translucent={true}
          barStyle="dark-content"
        />
        <Header androidStatusBarColor={'#0082f0'} style={styles.Header}>
          <Left>
            <TouchableOpacity onPress={() => Actions.drawerOpen()}>
              <Icon style={styles.IconHeader} name="menu" />
            </TouchableOpacity>
          </Left>
          <Right>
            <Text style={styles.TextHeader}>MITIGATE</Text>
          </Right>
        </Header>
        <View style={styles.HeaderWrapper}>
          {this.state.loadingHeader && (
            <ActivityIndicator size="large" color="#0082f0" />
          )}
          {!this.state.loadingHeader && (
            <FlatList
              showsHorizontalScrollIndicator={false}
              data={this.state.Categories}
              renderItem={this.renderHeader.bind(this)}
              keyExtractor={item => JSON.stringify(item.CategoryId)}
              horizontal={true}
              style={styles.FlatList}
            />
          )}
        </View>
        <Content style={styles.ContentStyle}>
          <View style={styles.Carousel}>
            <View style={styles.WrapperHeadCarousel}>
              <Text style={styles.HeadCarousel}>Today's Deals</Text>
            </View>
            {this.state.loadingTodayAds && (
              <ActivityIndicator size="large" color="#0082f0" />
            )}
            {!this.state.loadingTodayAds && (
              <Carousel
                ref={c => {
                  this._carousel = c;
                }}
                data={this.state.TodayAds}
                renderItem={this._renderItem.bind(this)}
                sliderWidth={width}
                itemWidth={180}
                style={styles.CarouselWrapper}
                firstItem={2}
                loop={true}
              />
            )}
          </View>
          <View style={styles.Carousel}>
            <View style={styles.WrapperHeadCarousel}>
              <Text style={styles.HeadCarousel}>Top</Text>
            </View>
            {this.state.loadingMostOffer && (
              <ActivityIndicator size="large" color="#0082f0" />
            )}
            {!this.state.loadingMostOffer && (
              <Carousel
                ref={c => {
                  this._carousel = c;
                }}
                data={this.state.MostOffer}
                renderItem={this._renderItem.bind(this)}
                sliderWidth={width}
                itemWidth={180}
                firstItem={2}
                loop={true}
              />
            )}
          </View>
          <View style={styles.Carousel}>
            <View style={styles.WrapperHeadCarousel}>
              <Text style={styles.HeadCarousel}>Bestseller</Text>
            </View>
            {this.state.loadingBestAds && (
              <ActivityIndicator size="large" color="#0082f0" />
            )}
            {!this.state.loadingBestAds && (
              <Carousel
                ref={c => {
                  this._carousel = c;
                }}
                data={this.state.BestAds}
                renderItem={this._renderItem.bind(this)}
                sliderWidth={width}
                itemWidth={180}
                firstItem={2}
                loop={true}
              />
            )}
          </View>
        </Content>
      </Container>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    setUser: user => {
      dispatch(setUser(user));
    },
  };
};

const mapStateToProps = state => {
  return {
    user: state.user,
    global: state.global,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Main);
