import React from 'react';
import {
  StatusBar,
  TextInput,
  Text,
  TouchableOpacity,
  View,
  ActivityIndicator,
} from 'react-native';
import {Container} from 'native-base';
import styles from '../assets/styles/AddCodeStyle';
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';
import {setUser} from '../redux/Actions/Index';
import LinearGradient from 'react-native-linear-gradient';
import AsyncStorage from '@react-native-community/async-storage';
import Modal from 'react-native-modal';
class AddCode extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      Code: '',
      Email: '',
      IsModal: false,
      Loading: false,
      hasError: false,
      errorDesc: '',
    };
  }
  async SendCode() {
    this.setState({
      IsModal: true,
      Loading: true,
      hasError: false,
      errorDesc: '',
    });
    try {
      let formData = new FormData();

      formData = {Email: this.props.Email, ActiveCode: this.state.Code};

      let response = await fetch(
        this.props.global.baseApiUrl + '/ActiveNormalUser',
        {
          method: 'POST',
          body: JSON.stringify(formData),
          headers: {
            'Content-Type': 'application/json',
          },
        },
      );
      let json = await response.json();
      console.log(json);
      console.log(response.status);
      if (response.status === 200) {
        await AsyncStorage.setItem('UserToken', json.Token);
        await AsyncStorage.setItem('role', JSON.stringify(json.RoleId));
        await this.props.setUser({
          apiToken: json.Token,
          role: json.RoleId,
        });
        Actions.replace('root');
      } else {
        this.setState({
          IsModal: true,
          Loading: false,
          hasError: true,
          errorDesc: 'FAILED',
        });
      }
    } catch (error) {
      this.setState({
        IsModal: true,
        Loading: false,
        hasError: true,
        errorDesc: 'FAILED',
      });
    }
  }
  render() {
    return (
      <Container style={styles.mainContainer}>
        <StatusBar
          backgroundColor="transparent"
          barStyle="dark-content"
          translucent={true}
        />
        <View
          style={{
            width: '100%',
            height: '100%',
            backgroundColor: '#fff',
            flex: 1,
            position: 'absolute',
          }}
        />
        <View style={styles.slide}>
          <View style={styles.inputContainer}>
            <Text style={styles.inputLabel}>Code verification</Text>
            <TextInput
              keyboardType="numeric"
              value={this.state.Code}
              underlineColorAndroid="rgba(0,0,0,0)"
              style={styles.inputMain}
              onChangeText={text => this.setState({Code: text})}
            />
          </View>
          <TouchableOpacity
            style={styles.colorGrButton}
            activeOpacity={0.8}
            onPress={() => this.SendCode()}>
            <LinearGradient
              colors={['#3399ff', '#fff']}
              style={styles.grButtonGradient}
              start={{x: 0, y: 1}}
              end={{x: 1, y: 1}}>
              <Text style={styles.grButtonText}>Submit</Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>
        <Modal
          onBackdropPress={() => this.setState({IsModal: false})}
          onBackButtonPress={() => this.setState({IsModal: false})}
          animationIn="fadeIn"
          animationOut="fadeOut"
          isVisible={this.state.IsModal}>
          <View style={styles.ModalError}>
            {this.state.Loading && (
              <View>
                <ActivityIndicator size="large" color="#0082f0" />
                <Text style={styles.waitText}>Please Wait ...</Text>
              </View>
            )}
            {this.state.hasError && (
              <View>
                <Text style={styles.TextDsc}>{this.state.errorDesc}</Text>
              </View>
            )}
          </View>
        </Modal>
      </Container>
    );
  }
}
const mapDispatchToProps = dispatch => {
  return {
    setUser: user => {
      dispatch(setUser(user));
    },
  };
};

const mapStateToProps = state => {
  return {
    user: state.user,
    global: state.global,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AddCode);
