import React from 'react';
import {
  StatusBar,
  View,
  ActivityIndicator,
  TouchableOpacity,
  Text,
} from 'react-native';
import {Container, Content, Icon} from 'native-base';
import styles from '../assets/styles/HomeStyle';
import {setUser} from '../redux/Actions/Index';
import {connect} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import {Actions} from 'react-native-router-flux';
class Splash extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      Loading: true,
    };
  }
  async GetData() {
    try {
      const token = await AsyncStorage.getItem('UserToken');
      const Role = await AsyncStorage.getItem('role');
      console.log(token);
      if (token === null) {
        this.setState({Loading: false});
      } else {
        this.setState({Loading: false});
        await this.props.setUser({
          apiToken: token,
          role: Role,
        });
        Actions.replace('root');
      }
    } catch (error) {
      console.log(error);
    }
  }
  componentDidMount() {
    this.GetData();
  }

  render() {
    return (
      <Container style={styles.mainContainer}>
        <StatusBar
          backgroundColor="transparent"
          translucent={true}
          barStyle="dark-content"
        />
        <View
          style={{
            backgroundColor: '#fff',
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          {this.state.Loading && <ActivityIndicator />}
          {!this.state.Loading && (
            <View>
              <Text>Choose Your Acount</Text>
              <View style={styles.choose}>
                <TouchableOpacity
                  style={styles.chooseItem}
                  onPress={() => Actions.push('register')}>
                  <Icon name="ios-cash" />
                  <Text>Seller</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.chooseItem}
                  onPress={() => Actions.push('login')}>
                  <Icon name="ios-person" />
                  <Text>User</Text>
                </TouchableOpacity>
              </View>
            </View>
          )}
        </View>
      </Container>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    setUser: user => {
      dispatch(setUser(user));
    },
  };
};

const mapStateToProps = state => {
  return {
    user: state.user,
    global: state.global,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Splash);
