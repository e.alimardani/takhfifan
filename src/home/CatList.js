import React from 'react';
import {
  Text,
  TouchableOpacity,
  ActivityIndicator,
  FlatList,
  View,
  Image,
} from 'react-native';
import {Content, Container, Header, Right, Left, Icon} from 'native-base';
import styles from '../assets/styles/CatListStyle';
import {setUser} from '../redux/Actions/Index';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
class CatList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      lang: 1,
      loading: true,
      List: [],
    };
  }
  componentDidMount() {
    console.log(this.props);
    this.GetList();
  }
  async GetList() {
    try {
      let formData = new FormData();
      formData = {
        CategoryId: this.props.Data.CategoryId,
        LanguageId: this.state.lang,
      };
      let response = await fetch(
        this.props.global.baseApiUrl + '/ShowAdsWithCategory',
        {
          method: 'POST',
          body: JSON.stringify(formData),
          headers: {
            'Content-Type': 'application/json',
          },
        },
      );
      let json = await response.json();
      console.log(json);
      console.log(formData);
      this.setState({
        loading: false,
        List: json,
      });
    } catch (error) {
      this.setState({
        loading: false,
      });
    }
  }
  ListEmptyComment() {
    if (this.state.loading) {
      return (
        <View>
          <ActivityIndicator color="#0082f0" size="large" />
        </View>
      );
    } else {
      return (
        <View style={styles.Empty}>
          <Text style={styles.TextEmpty}>Empty !</Text>
        </View>
      );
    }
  }
  renderItem({item}) {
    return (
      <TouchableOpacity
        onPress={() =>
          Actions.push('detail', {
            Data: item,
            DataHeader: this.props.DataHeader,
          })
        }
        style={styles.WrapperList}>
        <Image
          source={{
            uri: 'http://luxuryglass.ir/Images/Ads/Img/' + item.MainImg,
          }}
          style={styles.ImgList}
        />
        <View style={styles.ListData}>
          <View style={styles.WrapperTitle}>
            <Text style={styles.Title}>{item.Title}</Text>
            <Text style={styles.OffText}>{item.Percent + '%'}</Text>
          </View>
          <View style={styles.WrapperPrice}>
            <View style={styles.Price}>
              <Text style={styles.PriceNew}>{item.NewPrice + ' $'}</Text>
              <Text style={styles.PriceOld}>{item.OldPrice + ' $'}</Text>
            </View>
            <View style={styles.BuyCount}>
              <Text style={styles.BuyCountText}>{item.Countbuy + ' Buy'}</Text>
              <Icon style={styles.BuyCountIcon} name="cart" />
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
  render() {
    return (
      <Container style={styles.mainContainer}>
        <Header androidStatusBarColor={'#0082f0'} style={styles.Header}>
          <Left>
            <TouchableOpacity onPress={() => Actions.pop()}>
              <Icon style={styles.IconHeader} name="ios-arrow-back" />
            </TouchableOpacity>
          </Left>
          <Right>
            <Text style={styles.TextHeader}>
              {this.props.Data.CategoryName}
            </Text>
          </Right>
        </Header>
        <Content style={styles.ContentStyle}>
          <FlatList
            data={this.state.List}
            renderItem={this.renderItem.bind(this)}
            keyExtractor={item => JSON.stringify(item.AdsId)}
            ListEmptyComponent={this.ListEmptyComment.bind(this)}
          />
        </Content>
      </Container>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    setUser: user => {
      dispatch(setUser(user));
    },
  };
};

const mapStateToProps = state => {
  return {
    user: state.user,
    global: state.global,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CatList);
