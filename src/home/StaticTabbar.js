import * as React from 'react';
import {
  View,
  StyleSheet,
  TouchableWithoutFeedback,
  Animated,
  Dimensions,
} from 'react-native';
import {Icon} from 'native-base';
import {connect} from 'react-redux';
import {changeIndex} from '../redux/Actions/Index';

const {width} = Dimensions.get('window');

interface Tab {
  name: string;
}

interface StaticTabbarProps {
  tabs: Tab[];
  value: Animated.Value;
}

class StaticTabbar extends React.PureComponent {
  values: Animated.Value[] = [];

  constructor(props) {
    super(props);
    const {tabs} = this.props;
    this.values = tabs.map(
      (tab, index) => new Animated.Value(index === 2 ? 1 : 0),
    );
  }

  onPress = data => {
    const {value, tabs} = this.props;
    const tabWidth = width / tabs.length;
    return Animated.sequence([
      Animated.parallel(
        this.values.map(v => {
          console.log(data);
          this.props.changeIndex({
            index: data,
          });
          return Animated.timing(v, {
            toValue: 0,
            duration: 100,
            useNativeDriver: true,
          });
        }),
      ),
      Animated.parallel([
        Animated.spring(value, {
          toValue: tabWidth * data,
          useNativeDriver: true,
          tension: 20,
        }),
        Animated.spring(this.values[data], {
          toValue: 1,
          useNativeDriver: true,
        }),
      ]),
    ]).start();
  };

  render() {
    const {tabs, value} = this.props;
    return (
      <View style={styles.container}>
        {tabs.map((tab, key) => {
          const tabWidth = width / tabs.length;
          const cursor = tabWidth * key;
          const opacity = value.interpolate({
            inputRange: [cursor - tabWidth, cursor, cursor + tabWidth],
            outputRange: [1, 0, 1],
            extrapolate: 'clamp',
          });
          const translateY = this.values[key].interpolate({
            inputRange: [0, 1],
            outputRange: [64, -20],
            extrapolate: 'clamp',
          });
          const opacity1 = this.values[key].interpolate({
            inputRange: [0, 1],
            outputRange: [0, 1],
            extrapolate: 'clamp',
          });
          return (
            <React.Fragment {...{key}}>
              <TouchableWithoutFeedback onPress={() => this.onPress(key)}>
                <Animated.View style={[styles.tab, {opacity}]}>
                  <Icon name={tab.name} style={{color: 'white'}} size={25} />
                </Animated.View>
              </TouchableWithoutFeedback>
              <Animated.View
                style={{
                  position: 'absolute',
                  top: -8,
                  left: tabWidth * key,
                  width: tabWidth,
                  height: 64,
                  justifyContent: 'center',
                  alignItems: 'center',
                  opacity: opacity1,
                  transform: [{translateY}],
                }}>
                <View style={styles.activeIcon}>
                  <Icon name={tab.name} style={{color: '#0082f0'}} size={25} />
                </View>
              </Animated.View>
            </React.Fragment>
          );
        })}
      </View>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    changeIndex: index => {
      dispatch(changeIndex(index));
    },
  };
};

export default connect(
  null,
  mapDispatchToProps,
)(StaticTabbar);

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  tab: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 58,
  },
  activeIcon: {
    backgroundColor: '#FFFF',
    width: 50,
    height: 50,
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
    elevation: 3,
  },
});
