import React from 'react';
import {
  StatusBar,
  TextInput,
  TouchableOpacity,
  FlatList,
  ActivityIndicator,
} from 'react-native';
import {Text, View, Container, Picker, Content} from 'native-base';
import styles from '../assets/styles/LoginStyle';
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';
import LinearGradient from 'react-native-linear-gradient';
import RequestModal from '../Shared/RequestModal';
import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';
import Geolocation from '@react-native-community/geolocation';
import Modal from 'react-native-modal';
const DATA = [
  {
    id: '1',
    title: '00',
  },
  {
    id: '2',
    title: '01',
  },
  {
    id: '3',
    title: '02',
  },
  {
    id: '4',
    title: '03',
  },
  {
    id: '5',
    title: '04',
  },
  {
    id: '6',
    title: '05',
  },
  {
    id: '7',
    title: '06',
  },
  {
    id: '8',
    title: '07',
  },
  {
    id: '9',
    title: '08',
  },
  {
    id: '10',
    title: '09',
  },
  {
    id: '11',
    title: '10',
  },
  {
    id: '12',
    title: '11',
  },
  {
    id: '13',
    title: '12',
  },
  {
    id: '14',
    title: '13',
  },
  {
    id: '15',
    title: '14',
  },
  {
    id: '16',
    title: '15',
  },
  {
    id: '17',
    title: '16',
  },
  {
    id: '18',
    title: '17',
  },
  {
    id: '19',
    title: '18',
  },
  {
    id: '20',
    title: '19',
  },
  {
    id: '21',
    title: '20',
  },

  {
    id: '22',
    title: '21',
  },
  {
    id: '23',
    title: '22',
  },
  {
    id: '24',
    title: '23',
  },
];
class Register extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      showModal: false,
      min: [],
      hasError: false,
      errorDesc: '',
      loading: false,
      lang: 1,
      Email: '',
      Phone: '',
      Description: '',
      TimeAnswerOpenMin: '',
      TimeAnswerOpenHour: '',
      TimeAnswerCloseMin: '',
      TimeAnswerCloseHour: '',
      DateAnswerOpenHour: '',
      DateAnswerClose: '',
      Lat: null,
      Long: null,
      Capacity: '',
      Address: '',
      region: {
        latitude: 35.715298,
        longitude: 51.404343,
        latitudeDelta: 0.08,
        longitudeDelta: 0.08,
      },
      marker: null,
      IsModal: false,
      Loading: false,
    };
  }
  componentWillMount() {
    this.CalcuteMin();
  }
  CalcuteMin() {
    for (let index = 0; index < 60; index++) {
      if (index < 10) {
        this.state.min.push('0' + index);
      } else {
        this.state.min.push(index);
      }
    }
  }

  async RegisterSeller() {
    this.setState({
      IsModal: true,
      Loading: true,
      hasError: false,
      errorDesc: '',
    });
    try {
      let formData = new FormData();
      formData = {
        Email: this.state.Email,
        DeviceId: '111',
        Phone: this.state.Phone,
        Description: this.state.Description,
        TimeAnswerOpen:
          this.state.TimeAnswerOpenHour + ':' + this.state.TimeAnswerOpenMin,
        TimeAnswerClose:
          this.state.TimeAnswerCloseHour + ':' + this.state.TimeAnswerCloseMin,
        DateAnswerOpen: this.state.DateAnswerOpen,
        DateAnswerClose: this.state.DateAnswerClose,
        Lat: this.state.Lat,
        Long: this.state.Long,
        Capasity: this.state.Capacity,
        Address: this.state.Address,
        LanguageId: this.state.lang,
      };
      let response = await fetch(
        this.props.global.baseApiUrl + '/RegisterSeller',
        {
          method: 'POST',
          body: JSON.stringify(formData),
          headers: {
            'Content-Type': 'application/json',
          },
        },
      );
      if (response.status === 200) {
        this.setState({
          IsModal: false,
          Loading: false,
          hasError: true,
          errorDesc: '',
        });
        await Actions.push('addCode', {Email: this.state.Email});
      } else {
        this.setState({
          IsModal: true,
          Loading: false,
          hasError: true,
          errorDesc: 'FAILED',
        });
      }
    } catch {
      this.setState({
        IsModal: true,
        Loading: false,
        hasError: true,
        errorDesc: 'FAILED',
      });
    }
  }
  renderItem({item}) {
    return (
      <TouchableOpacity
        onPress={() => this.setState({TimeAnswerOpenHour: item.title})}>
        <Text
          style={
            this.state.TimeAnswerOpenHour === item.title
              ? styles.TitleClockselect
              : styles.TitleClock
          }>
          {item.title}
        </Text>
      </TouchableOpacity>
    );
  }
  renderItemMin({item}) {
    return (
      <TouchableOpacity
        onPress={() => this.setState({TimeAnswerOpenMin: item})}>
        <Text
          style={
            this.state.TimeAnswerOpenMin === item
              ? styles.TitleClockselect
              : styles.TitleClock
          }>
          {item}
        </Text>
      </TouchableOpacity>
    );
  }
  renderItemClose({item}) {
    return (
      <TouchableOpacity
        onPress={() => this.setState({TimeAnswerCloseHour: item.title})}>
        <Text
          style={
            this.state.TimeAnswerCloseHour === item.title
              ? styles.TitleClockselect
              : styles.TitleClock
          }>
          {item.title}
        </Text>
      </TouchableOpacity>
    );
  }
  renderItemMinClose({item}) {
    return (
      <TouchableOpacity
        onPress={() => this.setState({TimeAnswerCloseMin: item})}>
        <Text
          style={
            this.state.TimeAnswerCloseMin === item
              ? styles.TitleClockselect
              : styles.TitleClock
          }>
          {item}
        </Text>
      </TouchableOpacity>
    );
  }
  render() {
    return (
      <Container style={styles.mainContainer}>
        <StatusBar
          backgroundColor="#fff"
          barStyle="dark-content"
          translucent={true}
        />
        <Content style={styles.Content}>
          <View style={styles.inputContainer}>
            <Text style={styles.inputLabel}>Email</Text>
            <TextInput
              underlineColorAndroid="rgba(0,0,0,0)"
              style={styles.inputMain}
              keyboardType="default"
              onChangeText={data => this.setState({Email: data})}
            />
          </View>
          <View style={[styles.inputContainer]}>
            <Text style={styles.inputLabel}>Address</Text>
            <TextInput
              underlineColorAndroid="rgba(0,0,0,0)"
              style={styles.inputMain}
              keyboardType="default"
              onChangeText={data => this.setState({Address: data})}
            />
          </View>
          <View style={styles.inputContainer}>
            <Text style={styles.inputLabel}>Phone</Text>
            <TextInput
              maxLength={11}
              underlineColorAndroid="rgba(0,0,0,0)"
              style={styles.inputMain}
              keyboardType="email-address"
              onChangeText={data => this.setState({Phone: data})}
            />
          </View>
          <View style={styles.inputContainer}>
            <Text style={styles.inputLabel}>Time Answer Open</Text>
            <View style={styles.timeWrapper}>
              <View>
                <FlatList
                  showsVerticalScrollIndicator={false}
                  data={DATA}
                  renderItem={this.renderItem.bind(this)}
                  keyExtractor={item => item.id}
                  style={styles.TimeflatList}
                />
              </View>
              <Text style={styles.Clock}> : </Text>
              <View>
                <FlatList
                  showsVerticalScrollIndicator={false}
                  data={this.state.min}
                  renderItem={this.renderItemMin.bind(this)}
                  keyExtractor={item => JSON.stringify(item)}
                  style={styles.TimeflatList}
                />
              </View>
            </View>
          </View>
          <View style={styles.inputContainer}>
            <Text style={styles.inputLabel}>Time Answer Close</Text>
            <View style={styles.timeWrapper}>
              <View>
                <FlatList
                  showsVerticalScrollIndicator={false}
                  data={DATA}
                  renderItem={this.renderItemClose.bind(this)}
                  keyExtractor={item => item.id}
                  style={styles.TimeflatList}
                />
              </View>
              <Text style={styles.Clock}> : </Text>
              <View>
                <FlatList
                  showsVerticalScrollIndicator={false}
                  data={this.state.min}
                  renderItem={this.renderItemMinClose.bind(this)}
                  keyExtractor={item => JSON.stringify(item)}
                  style={styles.TimeflatList}
                />
              </View>
            </View>
          </View>
          <View style={styles.inputContainerDate}>
            <Text style={styles.inputLabel}>Date Answer Open</Text>
            <Picker
              mode="dialog"
              selectedValue={this.state.DateAnswerOpen}
              style={styles.PickerDate}
              onValueChange={(itemValue, itemIndex) =>
                this.setState({DateAnswerOpen: itemValue})
              }>
              <Picker.Item label="ُSaturday" value="ُSaturday" />
              <Picker.Item label="Sunday" value="Sunday" />
              <Picker.Item label="Monday" value="Monday" />
              <Picker.Item label="Tuesday" value="Tuesday" />
              <Picker.Item label="Wendsday" value="Wendsday" />
              <Picker.Item label="Thursday" value="Thursday" />
              <Picker.Item label="Friday" value="Friday" />
            </Picker>
          </View>
          <View style={styles.inputContainerDate}>
            <Text style={styles.inputLabel}>Date Answer Close</Text>
            <Picker
              mode="dialog"
              selectedValue={this.state.DateAnswerClose}
              style={styles.PickerDate}
              onValueChange={(itemValue, itemIndex) =>
                this.setState({DateAnswerClose: itemValue})
              }>
              <Picker.Item label="ُSaturday" value="ُSaturday" />
              <Picker.Item label="Sunday" value="Sunday" />
              <Picker.Item label="Monday" value="Monday" />
              <Picker.Item label="Tuesday" value="Tuesday" />
              <Picker.Item label="Wendsday" value="Wendsday" />
              <Picker.Item label="Thursday" value="Thursday" />
              <Picker.Item label="Friday" value="Friday" />
            </Picker>
          </View>
          <View style={styles.inputContainer}>
            <Text style={styles.inputLabel}>Capacity</Text>
            <TextInput
              underlineColorAndroid="rgba(0,0,0,0)"
              style={styles.inputMain}
              maxLength={3}
              keyboardType="numeric"
              onChangeText={data => this.setState({Capacity: data})}
            />
          </View>
          <View style={styles.inputContainer}>
            <TouchableOpacity
              onPress={() => this.setState({isModalVisible: true})}>
              <Text>Show on map</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.inputContainer}>
            <Text style={styles.inputLabel}>Description</Text>
            <TextInput
              multiline={true}
              underlineColorAndroid="rgba(0,0,0,0)"
              style={styles.inputMainDsc}
              maxLength={11}
              onChangeText={data => this.setState({Description: data})}
            />
          </View>
          <TouchableOpacity
            style={styles.colorGrButton}
            activeOpacity={0.8}
            onPress={() => this.RegisterSeller()}>
            <LinearGradient
              colors={['#3399ff', '#fff']}
              style={styles.grButtonGradient}
              start={{x: 0, y: 1}}
              end={{x: 1, y: 1}}>
              <Text style={styles.grButtonText}>Register</Text>
            </LinearGradient>
          </TouchableOpacity>
        </Content>
        <Modal
          onBackButtonPress={() => this.setState({isModalVisible: false})}
          isVisible={this.state.isModalVisible}>
          <View style={{flex: 1}}>
            <MapView
              provider={PROVIDER_GOOGLE}
              region={this.state.region}
              showsUserLocation={true}
              style={styles.map}
              onPress={event =>
                this.setState({marker: event.nativeEvent.coordinate})
              }>
              {this.state.marker && <Marker coordinate={this.state.marker} />}
            </MapView>
            <TouchableOpacity
              onPress={() =>
                this.setState({
                  Lat: this.state.marker.latitude,
                  Long: this.state.marker.longitude,
                  isModalVisible: false,
                })
              }
              style={styles.DoneBtn}>
              <Text style={styles.DoneText}>Done</Text>
            </TouchableOpacity>
          </View>
        </Modal>
        <Modal
          onBackdropPress={() => this.setState({IsModal: false})}
          onBackButtonPress={() => this.setState({IsModal: false})}
          animationIn="fadeIn"
          animationOut="fadeOut"
          isVisible={this.state.IsModal}>
          <View style={styles.ModalError}>
            {this.state.Loading && (
              <View>
                <ActivityIndicator size="large" color="#0082f0" />
                <Text style={styles.waitText}>Please Wait ...</Text>
              </View>
            )}
            {this.state.hasError && (
              <View>
                <Text style={styles.TextDsc}>{this.state.errorDesc}</Text>
              </View>
            )}
          </View>
        </Modal>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.user,
    global: state.global,
  };
};

export default connect(
  mapStateToProps,
  null,
)(Register);
