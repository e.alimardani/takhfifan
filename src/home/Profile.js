import React from 'react';
import {StatusBar, Text, View, Image, TouchableOpacity} from 'react-native';
import {Container, Icon} from 'native-base';
import styles from '../assets/styles/ProfileStyle';
import {setUser} from '../redux/Actions/Index';
import {connect} from 'react-redux';
import {Actions} from 'react-native-router-flux';
class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <Container style={styles.mainContainer}>
        <StatusBar
          backgroundColor="transparent"
          translucent={true}
          barStyle="dark-content"
        />
        <View style={styles.content}>
          <Text style={styles.TextName}>Tommy Shelby</Text>

          {this.props.user.role === ('2' || 2) && (
            <TouchableOpacity
              onPress={() => Actions.push('myOrders')}
              style={styles.ProfileItem}>
              <Text style={styles.Font16}>My Orders</Text>
              <Icon style={styles.IconHeader} name="ios-arrow-forward" />
            </TouchableOpacity>
          )}
          {this.props.user.role === ('3' || 3) && (
            <TouchableOpacity
              onPress={() => Actions.push('myRequests')}
              style={styles.ProfileItem}>
              <Text style={styles.Font16}>My Requests</Text>
              <Icon style={styles.IconHeader} name="ios-arrow-forward" />
            </TouchableOpacity>
          )}

          <TouchableOpacity style={styles.ProfileItem}>
            <Text style={styles.Font16}>My Comments</Text>
            <Icon style={styles.IconHeader} name="ios-arrow-forward" />
          </TouchableOpacity>
          <TouchableOpacity style={styles.ProfileItem}>
            <Text style={styles.Font16}>other</Text>
            <Icon style={styles.IconHeader} name="ios-arrow-forward" />
          </TouchableOpacity>
        </View>
      </Container>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    setUser: user => {
      dispatch(setUser(user));
    },
  };
};

const mapStateToProps = state => {
  return {
    user: state.user,
    global: state.global,
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Profile);
