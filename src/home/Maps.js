import React from 'react';
import {StyleSheet, View, Dimensions, Text} from 'react-native';

import MapView, {Marker, PROVIDER_GOOGLE} from 'react-native-maps';
import {Icon} from 'native-base';
const {width, height} = Dimensions.get('window');

class Maps extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      region: {
        latitude: 35.715298,
        longitude: 51.404343,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
      },
      marker: null,
    };
  }
  render() {
    return (
      <View style={styles.container}>
        <MapView
          provider={PROVIDER_GOOGLE}
          region={this.state.region}
          style={styles.map}
          onPress={event =>
            this.setState({marker: event.nativeEvent.coordinate})
          }>
          {this.state.marker && <Marker coordinate={this.state.marker} />}
        </MapView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});

export default Maps;
