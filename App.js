import React from 'react';
import {BackHandler, ToastAndroid, I18nManager} from 'react-native';
import {Router, Scene, Actions, Stack, Drawer} from 'react-native-router-flux';
import {connect, Provider} from 'react-redux';
import RNRestart from 'react-native-restart';
import Home from './src/home/Home';
import store from './src/redux/Store/Index';
import Login from './src/home/Login';
import Register from './src/home/Register';
import AddCode from './src/home/AddCode';
import Splash from './src/home/Spalsh';
import Detail from './src/home/Detail';
import CatList from './src/home/CatList';
import Maps from './src/home/Maps';
import MyOrders from './src/home/MyOrders';
import MyRequests from './src/home/MyRequests';
import Add from './src/home/Add';
import Nav from './src/home/IndexScreen';
import MainDrawer from './src/home/MainDrawer';
import Rules from './src/home/Rules';
import AboutUs from './src/home/AboutUs';
import ContactUs from './src/home/ContactUs';

let backButtonPressedOnceToExit = false;

class App extends React.Component {
  constructor(props) {
    super(props);

    I18nManager.allowRTL(false);

    if (I18nManager.isRTL) {
      RNRestart.Restart();
    }
  }
  onBackPress() {
    if (backButtonPressedOnceToExit) {
      BackHandler.exitApp();
    } else {
      if (Actions.currentScene !== 'nav') {
        Actions.pop();
        return true;
      } else {
        backButtonPressedOnceToExit = true;
        ToastAndroid.show('To Exit App tap back again', ToastAndroid.SHORT);
        //setting timeout is optional
        setTimeout(() => {
          backButtonPressedOnceToExit = false;
        }, 2000);
        return true;
      }
    }
  }

  render() {
    const RouterWithRedux = connect()(Router);

    return (
      <Provider store={store}>
        <RouterWithRedux backAndroidHandler={this.onBackPress.bind(this)}>
          <Stack hideNavBar>
            <Scene key="splash" component={Splash} initial />
            <Scene key="login" component={Login} />
            <Scene key="register" component={Register} />
            <Scene key="addCode" component={AddCode} />
            <Drawer contentComponent={MainDrawer} key="root" hideNavBar>
              <Scene key="main" hideNavBar initial>
                <Scene key="nav" component={Nav} initial type="reset" />
                <Scene key="home" component={Home} />
                <Scene key="detail" component={Detail} />
                <Scene key="catList" component={CatList} />
                <Scene key="map" component={Maps} />
                <Scene key="myOrders" component={MyOrders} />
                <Scene key="myRequests" component={MyRequests} />
                <Scene key="add" component={Add} />
                <Scene key="contactUs" component={ContactUs} />
                <Scene key="aboutUs" component={AboutUs} />
                <Scene key="rules" component={Rules} />
              </Scene>
            </Drawer>
          </Stack>
        </RouterWithRedux>
      </Provider>
    );
  }
}

export default App;
